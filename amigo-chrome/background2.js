var accepted_messagetypes = {
  "browser_action":true, 
  "pairing": true,
  "refresh": true,
};

var applications = {
"eighttracks": 
  {
    "url":"http://*.8tracks.com/*",
    "browser_action": {
        "pause": {
            "type": "code",
            "value": "document.getElementById('player_pause_button').click();", 
        }, 
        "play": {
            "type": "code",
            "value": "if(document.getElementById('player_box')) {document.getElementById('player_pause_button').click();} else {document.getElementById('mix_details').click();}",
        }, 
        "skip": {
            "type": "code",
            "value": "document.getElementById('player_skip_button').click();",
        }
    },
    "android_card": {
      "data": {
        "song_name": {
          "type": "code",
          "value": "document.title",
        }
      }
    }
  },
// =========================================================================
"googleslides": 
  {
    "url":"https://docs.google.com/presentation/*",
    "browser_action": {
        "next": {
            "type": "code",
            "value": "var dispatchMouseEvent = function(target, var_args) { var e = document.createEvent('MouseEvents'); e.initEvent.apply(e, Array.prototype.slice.call(arguments, 1)); target.dispatchEvent(e); }; var element = document.getElementsByClassName('punch-present-iframe')[0].contentWindow.document.getElementsByClassName('punch-viewer-right')[0].parentNode; dispatchMouseEvent(element, 'mousedown', true, true); dispatchMouseEvent(element, 'mouseup', true, true); dispatchMouseEvent(element, 'mouseout', true, true);"
        }, 
        "previous": {
            "type": "code",
            "value": "var dispatchMouseEvent = function(target, var_args) { var e = document.createEvent('MouseEvents'); e.initEvent.apply(e, Array.prototype.slice.call(arguments, 1)); target.dispatchEvent(e); }; var element = document.getElementsByClassName('punch-present-iframe')[0].contentWindow.document.getElementsByClassName('punch-viewer-left')[0].parentNode; dispatchMouseEvent(element, 'mousedown', true, true); dispatchMouseEvent(element, 'mouseup', true, true); dispatchMouseEvent(element, 'mouseout', true, true);"
        }
    },
    "android_card": {},
  },
// =========================================================================
"grooveshark": 
  {
    "url":"http://grooveshark.com/*",
    "browser_action": {
        "skip": {
            "type": "code",
            "value": "document.getElementById('play-next').click();"
        }, 
        "pause": {
            "type": "code",
            "value": "document.getElementById('play-pause').click();"
        }, 
        "play": {
            "type": "code",
            "value": "document.getElementById('play-pause').click();"
        },
    },
    "android_card": {},
  },
// =========================================================================
"iheartradio": 
  {
    "url":"http://*.iheart.com/*",
    "browser_action": {
        "pause": {
            "type": "code",
            "value": "document.getElementsByClassName('btn-circle medium play')[0].click();"
        }, 
        "play": {
            "type": "code",
            "value": "document.getElementsByClassName('btn-circle medium play')[0].click();"
        },
    },
    "android_card": {},
  },
// =========================================================================
"netflix":
  {
    "url":"http://*.netflix.com/WiPlayer*",
    "browser_action": {
        "pause": {
            "type": "code",
            "value": "document.getElementsByClassName('player-control-button player-play-pause pause')[0].click()", 
        },
        "play": {
            "type": "code",
            "value": "document.getElementsByClassName('player-control-button player-play-pause play')[0].click();", 
        },
    },
    "android_card": {},
  },
// =========================================================================
"pandora": 
  {
    "url":"http://*.pandora.com/*",
    "browser_action": {
        "skip": {
            "type": "code",
            "value": "document.getElementsByClassName('skipButton')[0].click();",
        }, 
        "pause": {
            "type": "code",
            "value": "document.getElementsByClassName('pauseButton')[0].click();", 
        }, 
        "play": {
            "type": "code",
            "value": "document.getElementsByClassName('playButton')[0].click();",
        }, 
    },
    "android_card": {},
  },

// =========================================================================
"prezi": 
  {
    "url":"http://engineering.prezi.com/blog/2013/09/03/prezi-javascript-api/",
    "browser_action": {
        "next": {
            "type": "code",
            "value": "var inject = 'player.flyToNextStep();';" + inject()
        }, 
        "previous": {
            "type": "code",
            "value": "var inject = 'player.flyToPreviousStep();';" + inject()
        }
    },
    "android_card": {},
  },
// =========================================================================
"slideshare": 
  {
    "url":"http://*.slideshare.net/*",
    "browser_action": {
        "next": {
            "type": "code",
            "value": "document.getElementById('btnNext').click();"
        }, 
        "previous": {
            "type": "code",
            "value": "document.getElementById('btnPrevious').click();"
        }
    },
    "android_card": {},
  },
// =========================================================================
"songza": 
  {
    "url":"http://songza.com/*",
    "browser_action": {
        "skip": {
            "type": "code",
            "value": "document.getElementsByClassName('miniplayer-control-skip')[0].click();"
        }, 
        "pause": {
            "type": "code",
            "value": "document.getElementsByClassName('miniplayer-control-play-pause')[0].click();"
        }, 
        "play": {
            "type": "code",
            "value": "document.getElementsByClassName('miniplayer-control-play-pause')[0].click();"
        },
    },
    "android_card": {},
  },
// =========================================================================
"soundcloud": 
  {
    "url":"https://*.soundcloud.com/*",
    "browser_action": {
        "skip": {
            "type": "code",
            "value": "document.getElementsByClassName('skipControl sc-ir')[1].click();"
        }, 
        "pause": {
            "type": "code",
            "value": "document.getElementsByClassName('playControl sc-ir')[0].click();"
        }, 
        "play": {
            "type": "code",
            "value": "document.getElementsByClassName('playControl sc-ir')[0].click();"
        },
    },
    "android_card": {},
  },
// =========================================================================
"spotify": 
  {
    "url":"https://play.spotify.com/*",
    "browser_action": {
        "skip": {
            "type": "code",
            "value": "(document.getElementById('app-player').contentDocument || document.getElementById('app-player').contentWindow.document).getElementById('next').click();",
        }, 
        "pause": {
            "type": "code",
            "value": "(document.getElementById('app-player').contentDocument || document.getElementById('app-player').contentWindow.document).getElementById('play-pause').click();",

        }, 
        "play": {
            "type": "code",
            "value": "(document.getElementById('app-player').contentDocument || document.getElementById('app-player').contentWindow.document).getElementById('play-pause').click();",
        },
    },
    "android_card": {},
  },

// =========================================================================
"vimeo":
  {
    "url":"https://*.vimeo.com/*",
    "browser_action": {
        "pause": {
            "type": "code",
            "value": "document.getElementsByClassName('play rounded-box state-playing')[0].click();", 
        },
        "play": {
            "type": "code",
            "value": "document.getElementsByClassName('play rounded-box state-paused')[0].click();", 
        },
    },
    "android_card": {},
  },

// =========================================================================
"youtube":
  {
    "url":"https://*.youtube.com/watch*",
    "browser_action": {
        "pause": {
            "type": "code",
            "value": "document.getElementsByClassName('ytp-button-pause')[0].click();", 
        },
        "play": {
            "type": "code",
            "value": "document.getElementsByClassName('ytp-button-play')[0].click();", 
        },
    },
    "android_card": {},
  },

// =========================================================================
"yelp":
  {
    "url":"http://*.yelp.com/*",
    "android_card": {
        "required": ['latlng'],
        "data": {
            "type": "getData",
            "value": stringify(
              function(){
                var JSONString = (document.getElementsByClassName('lightbox-map hidden'))[0].getAttribute('data-map-state');
                yelpObject = JSON.parse(JSONString);
                return {'latlng': yelpObject['center']['latitude'] + ',' + yelpObject['center']['longitude']}
              }),
       }
    }
  },  
// =========================================================================
"docs":
  {
    "url":"https://docs.google.com/document/d/*",
    "browser_action": {
      "place_image": {
        "type": "function",
        "required": ["image_src"],
        "value": "(" + function create_image (image_src, retries) {
                  console.log("retry " + retries)
                  // NEED TO INCLUDE STUFF THAT CHECKS FOR d
                  d = document.createElement("div");
                  p = document.createElement("img");
                  loading = document.createElement("img");

                  document.body.appendChild(d);
                  d.appendChild(loading);
                  d.appendChild(p);

                  p.src = image_src;
                  p.style.maxWidth = "25%";
                  p.style.opacity = 0;
                  p.style.WebkitTransition="opacity .5 ease-out 0";

                  d.style.position = "absolute";
                  d.style.top = 0;
                  d.style.left = 0;
                  d.style.zIndex = 9999;

                  loading.src = chrome.extension.getURL("loading.gif");

                  p.addEventListener("dragleave", function remove_image() {
                    // NEED TO INCLUDE STUFF THAT DELETES d, AND ALSO ENABLES MULTIPLE IMAGES
                    p.remove();
                  }, false);

                  // retry for 10 seconds
                  p.onerror = function() {
                    if (retries < 20) {
                      setTimeout(function (){
                        p.src = "";
                        p.src = image_src;
                      }, 500);
                    } else {
                      remove_image();
                    }
                  }

                  p.onload = function() {
                    loading.remove();
                    p.style.opacity = 1;
                  }
                } + ")(image_src, 0);",
      }
    },
    "android_card": {},
  },
}

var lastTime = (new Date()).getTime();

setInterval(function() {
  var currentTime = (new Date()).getTime();
  if (currentTime > (lastTime + 10000*2)) {  // ignore small delays
    // Probably just woke up!
    checkTabs();
  }
  lastTime = currentTime;
}, 10000);

function stringify(funct) {
  return "(" + funct + ")();"
}

// get parameters from function
var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
var ARGUMENT_NAMES = /([^\s,]+)/g;
function getParamNames(func) {
  var fnStr = func.toString().replace(STRIP_COMMENTS, '');
  var result = fnStr.slice(fnStr.indexOf('(')+1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
  if(result === null)
     result = [];
  return result;
}

function addParamsToFunction(action, message) {
  var stringBuilder = "";

  if (action["type"] == "function") {
    action["required"].forEach(function(paramName) {
      if (paramName in message) {
        stringBuilder += "var " + paramName + "=\"" + message[paramName] + "\";";
      } else {
        console.log("Incorrect parameter names! Looking for " + paramName);
        return
      }
    });
  }

  return stringBuilder + action["value"];
}

// returns a serialized function that injects code into the local js environment
// requires a variable to be set named "inject"
function inject() {
  return "(" + function() {
    if (!inject) {
      console.log("Empty inject");
      return;
    }
    var script = document.createElement('script');
    script.textContent = inject;
    (document.head||document.documentElement).appendChild(script);
    script.parentNode.removeChild(script);
  } + ")();"
}

// ============================================================================
// ============================================================================
// MESSAGE RECEIVED

function messageReceived(fullMessage) {
  message = fullMessage.data;
  printMessage(message);

  var messagetype = message["messagetype"];
  var deviceid = message["deviceid"];
  if(!messagetype || !accepted_messagetypes[messagetype]) {
    console.log("Unknown message type");
    return;
  }
  if(!deviceid) {
    console.log("No deviceid in message");
    return;
  }

  // Successful Pairing callback
  if (messagetype == "pairing") {
    console.log("Successful pairing!");
    chrome.storage.local.set({paired: true, deviceid: deviceid});
    mydeviceid = deviceid;
    // let the android know about the currently open tabs.
    onStartup();

    // // adjust the chrome extension page if possible
    // chrome.tabs.query({url: "chrome-extension://*/welcome.html*"}, function(tabs) {
    //   if (tabs) {
    //     console.log("data's url is " + data["url"]);
    //     var appsPrevTabs = previousTabs[message["application"]];
    //     performAction(appsPrevTabs[appsPrevTabs.length - 1].id, action);
    //   }
    // });

    return;
  }

  // deviceid must match to continue
  if (mydeviceid != deviceid) {
    console.log("deviceid does not match! " + deviceid + " " + mydeviceid);
    return;
  }

  // android doesn't know anything. Send them all the data.
  if (messagetype == "refresh") {
    console.log("Refresh message received.");
    resetPreviousTabs();
    checkTabs();
  }

  // browser action
  else { 
    var data = applications[message["application"]];
    console.log(data[message["messagetype"]])
    var action = data[message["messagetype"]][message["action"]]

    // execute script in background tab
    chrome.tabs.query({url: data["url"]}, function(tabs) {

      if (tabs) {
        console.log("data's url is " + data["url"]);
        var appsPrevTabs = previousTabs[message["application"]];
        performAction(appsPrevTabs[appsPrevTabs.length - 1].id, action, message);
      }
    });
  }
}

function performAction(tabid, action, message) {
  if (action["type"] == "code") {
    chrome.tabs.executeScript(tabid, {code: action["value"]}, function(result) {
      console.log("Executed!", result);
    });
  } else if (action["type"] == "file") {
    chrome.tabs.executeScript(tabid, {file: action["value"]}, function(result) {
      console.log("Executed!", result);
    });
  } else if (action["type"] == "function") {
    // if the parameters were formated correctly
    console.log(action, message);
    if (code = addParamsToFunction(action, message)) {
      chrome.tabs.executeScript(tabid, {code: code},
        function(result) {
          console.log("Function Executed: " + code);
        });
    }
  }
}

// ============================================================================
// ============================================================================
// TEMP NOTE TAKING CODE

// will be stringified
var displayImageCode = function(image_src) {
  d = document.createElement("div");
  p = document.createElement("img");
  document.body.appendChild(d);
  d.appendChild(p);

  p.src = image_src;
  p.style.position = "absolute";
  p.style.top = 0;
  p.style.left = 0;
  p.zIndex = 9999;

  p.addEventListener("dragleave", function() {
    // NEED TO INCLUDE STUFF THAT DELETES d, AND ALSO ENABLES MULTIPLE IMAGES
    p.remove()
  }, false);
};

// REQURES ADDING
// "clipboardRead", "clipboardWrite", 
// TO MANIFEST
function addToClipboard(message) {
  var input = document.createElement('textarea');
  document.body.appendChild(input);
  input.value = message;
  input.focus();
  input.select();
  document.execCommand('Copy');
  input.remove();
}

// ============================================================================
// ============================================================================
// MESSAGE SENDER

// send all tabs to the android application
var previousTabs = {};
resetPreviousTabs();

function sendAndroidAddCardMessage(application, tab) {
  var title = tab.title;

  var data = {
    'application': application,
    'messagetype': "android_card",
    'deviceid': mydeviceid,
    'action': 'add',
    'title': title
  };
  
  var dataFetcher = applications[application]["android_card"]["data"]
  sendMessageWithData(tab.id, dataFetcher, data);
}

function sendAndroidUpdateCardMessage(application, tab) {
  var title = tab.title;
  console.log("updating a card");
  var data = {
    'application': application,
    'messagetype': "android_card",
    'deviceid': mydeviceid,
    'action': 'update',
    'title': title
  };
  
  var dataFetcher = applications[application]["android_card"]["data"]
  sendMessageWithData(tab.id, dataFetcher, data);
}

// fetches some extra data from the webpage before sending the message
function sendMessageWithData(tabid, action, data) {

  if (action && action["type"] == "getData") {
    // if extra data is needed
    chrome.tabs.executeScript(tabid, {code: action["value"]}, function(result) {
      console.log("Getting Data from page", result[0]);
      $.extend(data, result[0]);
      sendMessage(data);
    });
  } else {
    // no extra data is necessary
    sendMessage(data);
  }
}

function sendAndroidRemoveCardMessage(application) {
  sendMessage({
    'application': application,
    'messagetype': 'android_card',
    'deviceid': mydeviceid,
    'action': 'remove',
  });
}

function resetPreviousTabs() {
  previousTabs = {};
  for (var application in applications) {
    previousTabs[application] = [];
  }
}

function checkTabs() {
  for (var application in applications) {
    if (applications.hasOwnProperty(application)) {
      // create closure so that the async function refers to the proper value
      (function (application) {
        chrome.tabs.query({url: applications[application]['url']}, function(tabs) {
          //console.log("Querying tabs for " + applications[application]['url']);
          var removedtabId = -1;
          var addedtabId = -1;
          if (tabs.length > 0) {
            console.log("Number of tabs found for " + application + " : " + tabs.length)
            console.log("Number of tabs in previous for " + application + " : " + previousTabs[application].length)
          }
          if (previousTabs[application]==[] && tabs.length) {
            console.log("There was no tab of this kind before");
            var tab = tabs[0];
            previousTabs[application].push(tab);
            console.log("application", application, "tab number", tab.id);
            sendAndroidAddCardMessage(application, tab);
          }
          else if (tabs.length == previousTabs[application].length+1) {
            console.log("This is an additional tab of the same kind");
            var indexOfNewTab;
            for(i=0; i < tabs.length; i++) {
              if ($.inArray(tabs[i],previousTabs[application]) == -1) {
                addedtabId = tabs[i];
                indexOfNewTab = i;
              }
            }

            previousTabs[application].push(addedtabId);
            console.log("application", application, "tab number", addedtabId, "index of new tab", indexOfNewTab);
            sendAndroidUpdateCardMessage(application, tabs[indexOfNewTab]);
          }
          // if application was removed from tabs
          else if (tabs.length == previousTabs[application].length-1) {
            console.log("A tab was removed and we're handling it");            
            if (previousTabs[application]) {
              for(i in previousTabs[application]) {
                if ($.inArray(i,tabs) == -1) {
                  removedtabId = i;
                }
              }
              previousTabs[application].splice(previousTabs[application].indexOf(removedtabId), 1);
              console.log("application", application, "tab number", addedtabId, "removedTabId", removedtabId);

              if (previousTabs[application].length == 0) {
                sendAndroidRemoveCardMessage(application)
              }
              else {
                console.log("A tab was removed and we are now updating the card.");
                var appsPrevTabs = previousTabs[application];
                sendAndroidUpdateCardMessage(application, appsPrevTabs[appsPrevTabs.length-1]);
              }
            }
          }
        });
      } (application));
    }
  }
}



function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function checkUpdatedTabs(tabIdNOTUSED, changeInfoNOTUSED, changedTab) {
  for (var application in applications) {
    if (applications.hasOwnProperty(application)) {
      // create closure so that the async function refers to the proper value
      (function (application) {
        chrome.tabs.query({url: applications[application]['url']}, function(tabs) {
          //console.log("Querying tabs for " + applications[application]['url']);
          var removedtabId = -1;
          var addedtabId = -1;

          //if there are tabs (debugging)
          if (tabs.length > 0) {
            console.log("!Number of tabs found for " + application + " : " + tabs.length)
            console.log("Number of tabs in previous for " + application + " : " + previousTabs[application].length)
          }
          //if there are no tabs of this kind before
          //note, this if statement may never even get run...
          if (previousTabs[application]==[] && tabs.length) {
            console.log("There was no tab of this kind before");
            var tab = tabs[0];
            previousTabs[application].push(tab);
            console.log("application", application, "tab number", tab.id);
            sendAndroidAddCardMessage(application, tab);
          }
          //if we are adding a new tab of the same kind
          //note, this may always get run instead of the previous if statement (not a problem)
          else if (tabs.length == previousTabs[application].length+1) {
            console.log("This is an additional tab of the same kind");
            var indexOfNewTab;
            for(i=0; i < tabs.length; i++) {
              if ($.inArray(tabs[i],previousTabs[application]) == -1) {
                addedtabId = tabs[i];
                indexOfNewTab = i;
              }
            }

            previousTabs[application].push(addedtabId);
            console.log("application", application, "tab number", addedtabId, "index of new tab", indexOfNewTab);
            sendAndroidUpdateCardMessage(application, tabs[indexOfNewTab]);
          }
          //this runs if the # of tabs is the same, but a particular tab changed
          else if (tabs.length == previousTabs[application].length) {
            //console.log("Same amount of tabs. ChangedTab = " + changedTab.id )
            for (i = 0; i < previousTabs[application].length; i++) {
              if (previousTabs[application][i].id == changedTab.id) {
                console.log("A tab's url was changed, so we are updating the android card.")
                previousTabs[previousTabs[application].indexOf(i)] = changedTab
                sleep(100); //necessary to allow update of title
                //getting the refreshed title
                chrome.tabs.get(changedTab.id, function(tab) { 
                  sendAndroidUpdateCardMessage(application, tab); 
                })
              }
            }
          }
          // if application was removed from tabs
          else if (tabs.length == previousTabs[application].length-1) {
            console.log("A tab was removed and we're handling it");            
            if (previousTabs[application]) {
              for(i in previousTabs[application]) {
                if ($.inArray(i,tabs) == -1) {
                  removedtabId = i;
                }
              }
              previousTabs[application].splice(previousTabs[application].indexOf(removedtabId), 1);
              console.log("application", application, "tab number", addedtabId, "removedTabId", removedtabId);

              if (previousTabs[application].length == 0) {
                sendAndroidRemoveCardMessage(application)
              }
              else {
                console.log("A tab was removed and we are now updating the card.")
                sendAndroidUpdateCardMessage(application, previousTabs[application][previousTabs[application].length-1]);
              }
            }
          }
        });
      } (application));
    }
  }
}

// only starts listening when registered
// listeners for updating the android app
function onStartup() {
  checkTabs();
  chrome.tabs.onRemoved.addListener(weakDelete);
  chrome.tabs.onUpdated.addListener(checkUpdatedTabs);
}

function weakDelete(tabid, removeinfo) {
  
  for (application in previousTabs) {
    match = false;
    for (j in previousTabs[application]) {
      if (previousTabs[application][j].id == tabid) {
        previousTabs[application].splice(j, 1);
        match = true;
        break;
      }
    }
    if (match == true) {


      console.log("Weak Delete.");

      // if application was removed from tabs
      console.log("=====A tab was removed and we're handling it======");      

      //found a nonexistant tab, remove it 
      console.log("application", application, "removedTabId", i);

      if (previousTabs[application].length == 0) {
        sendAndroidRemoveCardMessage(application)
      }
      else {
        console.log("A tab was removed and we are now updating the card.")
        sendAndroidUpdateCardMessage(application, previousTabs[application][previousTabs[application].length-1]);
      }
    }
  }   
}
// ============================================================================
// ============================================================================
// PRIVATE METHODS

var displayPopups = false;

function debugModeOn() {
  displayPopups = true;
}
function debugModeOff() {
  displayPopups = false;
}

// Returns a new notification ID used in the notification.
function getNotificationId() {
  var id = Math.floor(Math.random() * 9007199254740992) + 1;
  return id.toString();
}

// displays the contents of the message in the console and via popup
function printMessage(message) {
  var messageString = "";
  for (var key in message) {
    if (messageString != "")
      messageString += ", "
    messageString += key + ":" + message[key];
  }
  console.log("Message received: " + messageString);

  // Pop up a notification to show the GCM message.
  if (displayPopups) {
    chrome.notifications.create(getNotificationId(), {
      title: 'GCM Message',
      iconUrl: 'logo.png',
      type: 'basic',
      message: messageString
    }, function() {});
  }

  console.log(message);
}

// ============================================================================
// ============================================================================
// GCM REGISTRATION / PAIRING

// Set up listeners to trigger the first time registration.
chrome.runtime.onInstalled.addListener(firstTimeRegistration);
chrome.runtime.onStartup.addListener(firstTimeRegistration);

function firstTimeRegistration() {
  chrome.storage.local.get("paired", function(result) {
    // If not already registered, register
    if (!result["paired"]) {
      console.log("Needs to register and pair.")
      register();
    }
    else {
      // android will ask chrome for the update
      console.log("Already registered.")
      onStartup();
    }
  });
}

// used to locally store the deviceid
var mydeviceid;
chrome.storage.local.get("deviceid", function(result) {
  mydeviceid = result["deviceid"];
});

// Substitute your own sender ID here. This is the project
// number you got from the Google Developers Console.
var senderId = ["596039776570"];

function register() {
  chrome.gcm.register(senderId, function(regId) {

    console.log("Registration ID of extension: " + regId);

    if (chrome.runtime.lastError) {
      // When the registration fails, handle the error and retry the
      // registration later.
      console.log(chrome.runtime.lastError.message);
      return;
    }

    // Mark that the first-time registration is done.
    pin = (Math.floor(Math.random() * 9000) + 1000).toString();

    //PROBLEMATIC: WHAT HAPPENS IF WE ACCIDENTALLY GENERATE THE SAME RANDOM NUMBER?


    // only one of these has to succeed
    setTimeout(function() { updateRegID(regId, pin) }, 2000);
    setTimeout(function() { updateRegID(regId, pin) }, 5000);
    setTimeout(function() { updateRegID(regId, pin) }, 10000);
    
    //open html in a new tab with the secret code
    // delay by 2 seconds. Then delay the reveal of the key by 4 seconds
    setTimeout(function() { window.open("/welcome.html?yourKey=" + pin); }, 2000);
    //w.pin = testObject;
  });
}

// Send the registration message
function updateRegID(regId, pin) {
  var updateData = {
    'messagetype': "pairing",
    'action': "start",
    'devicetype': 'chrome',
    'pin': pin,
  };
  console.log("Registration message to be sent to server:");
  console.log(updateData);
  sendMessage(updateData);
}

// Set up a listener for GCM message event.
chrome.gcm.onMessage.addListener(messageReceived);

// Make the message ID unique across the lifetime of your app.
// One way to achieve this is to use the auto-increment counter
// that is persisted to local storage.

// Message ID is saved to and restored from local storage.
var messageId = 0;
chrome.storage.local.get("messageId", function(result) {
  if (chrome.runtime.lastError)
    return;
  messageId = parseInt(result["messageId"]);
  if (isNaN(messageId))
    messageId = 0;
});

// Returns a new ID to identify the message.
function getMessageId() {
  messageId++;
  chrome.storage.local.set({messageId: messageId});
  return messageId.toString();
}

function sendMessage(data) {

  var message = {
    'messageId': getMessageId(),
    'destinationId': senderId + "@gcm.googleapis.com",
    'timeToLive': 86400,    // 1 day
    'data': data
  };

console.log("Sending message", message, data);

  chrome.gcm.send(message, function(messageId) {
    if (chrome.runtime.lastError) {
      // Some error occurred. Fail gracefully or retry
      return;
    }
  });
}

// If the message can not reach the destination, onSendError
// event will be fired.
chrome.gcm.onSendError.addListener(sendError);

function sendError(error) {
  console.log("Message " + error.messageId +
      " failed to be sent: " + error.errorMessage);
}
