import sqlite3
conn = sqlite3.connect('registrations.db')
c = conn.cursor()
c.execute('''CREATE TABLE users (
	userid text PRIMARY KEY,
	sentToAndroid integer,
	sentToChrome integer
	)''')
c.execute('''CREATE TABLE devices (
	deviceid text PRIMARY KEY,
	regid text,
	devicetype text,
	userid text,
	FOREIGN KEY(userid) REFERENCES users(userid)
	)''')