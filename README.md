amico
=====
ERRORS:
I have identified an error with the following behavior:
Chrome extension connects to server
When android app opened and number typed in, app thinks it has connected but the server prints a message saying "recipient NONE". I have explored the options and it appears that the server is not correctly storing/saving the id numbers of the clients. I have investigated the usage of the "INSERT INTO" command and it appears to be correct.



ssh -i [path to keypair.pem] ec2-user@ec2-54-172-238-74.compute-1.amazonaws.com
 
MAKE SURE YOU DO THIS
  source venv/bin/activate

TO RUN IN BACKGROUND
  nohup command >/dev/null 2>&1 &
TO STOP RUNNING IN BACKGROUND
  kill -9 `cat save_pid.txt`
  rm save_pid.txt


NAMING PROTOCOLS:
//sites
eighttracks
prezi
vimeo

HOW TO ADD A NEW APP:
---CHROME---
background2.js - add to the json-formatted part
---SERVER---
script.py - add to the json-formatted part
---ANDROID---
GCMBroadcastReceiver.java: add and remove notification
MainActivity.java: add onClick function and card stuff
NotifyActivityHandler.java: add a bit of logic
layout: add an xml file for row_card

//url
url

//music
pause
play
skip

//presentation
next
previous

//video
pauseVid
playVid
