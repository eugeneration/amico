package com.example.stephencognetta.amico;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class NotifyActivityHandler extends IntentService {
    public static final String PERFORM_NOTIFICATION_BUTTON = "perform_notification_button";

    public NotifyActivityHandler() {
        super("NotifyActivityHandler");
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        Log.v("Notification", "Intent: " + intent.getExtras());
        Bundle extras = intent.getExtras();


        // Go to main activity
        if(intent.getExtras().isEmpty()) {
            startActivity(intent);
        }
        // Perform action
        else {
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

            if (intent.getExtras().containsKey("methodName")) {
                Log.v("Notification", "About to execute a method based on notification.");
                String method = intent.getStringExtra("methodName");
                String action = intent.getStringExtra("action");
                String deviceid = intent.getStringExtra("deviceid");

                if(action.equals("remove")) {
                    android.app.NotificationManager mNotifyMgr =
                            (android.app.NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    if (extras.get("methodName").equals(MainActivity.EIGHTTRACKS.nameAbbrev)) {
                        mNotifyMgr.cancel(MainActivity.EIGHTTRACKS.notificationId);
                    }
                    else if (extras.get("methodName").equals(MainActivity.PANDORA.nameAbbrev)) {
                        mNotifyMgr.cancel(MainActivity.PANDORA.notificationId);
                    }
                    else if (extras.get("methodName").equals(MainActivity.YOUTUBE.nameAbbrev)) {
                        mNotifyMgr.cancel(MainActivity.YOUTUBE.notificationId);
                    }
                    else if (extras.get("methodName").equals(MainActivity.VIMEO.nameAbbrev)) {
                        mNotifyMgr.cancel(MainActivity.VIMEO.notificationId);
                    }
                    else if (extras.get("methodName").equals(MainActivity.PREZI.nameAbbrev)) {
                        mNotifyMgr.cancel(MainActivity.PREZI.notificationId);
                    }
                    else if (extras.get("methodName").equals(MainActivity.SPOTIFY.nameAbbrev)) {
                        mNotifyMgr.cancel(MainActivity.SPOTIFY.notificationId);
                    }
                    else if (extras.get("methodName").equals(MainActivity.NETFLIX.nameAbbrev)) {
                        mNotifyMgr.cancel(MainActivity.NETFLIX.notificationId);
                    }
                    else if (extras.get("methodName").equals(MainActivity.IHEARTRADIO.nameAbbrev)) {
                        mNotifyMgr.cancel(MainActivity.IHEARTRADIO.notificationId);
                    }
                    else if (extras.get("methodName").equals(MainActivity.SLIDESHARE.nameAbbrev)) {
                        mNotifyMgr.cancel(MainActivity.SLIDESHARE.notificationId);
                    }
                    else if (extras.get("methodName").equals(MainActivity.GOOGLESLIDES.nameAbbrev)) {
                        mNotifyMgr.cancel(MainActivity.SLIDESHARE.notificationId);
                    }
                    else if (extras.get("methodName").equals(MainActivity.SONGZA.nameAbbrev)) {
                        mNotifyMgr.cancel(MainActivity.SONGZA.notificationId);
                    }
                    else if (extras.get("methodName").equals(MainActivity.SOUNDCLOUD.nameAbbrev)) {
                        mNotifyMgr.cancel(MainActivity.SOUNDCLOUD.notificationId);
                    }
                    else if (extras.get("methodName").equals(MainActivity.GROOVESHARK.nameAbbrev)) {
                        mNotifyMgr.cancel(MainActivity.GROOVESHARK.notificationId);
                    }
                    else  {

                    }
                }
                GcmSender.ActionMessage message = new GcmSender.ActionMessage();
                message.application = method;
                message.type = "browser_action";
                message.action = action;
                message.deviceid = deviceid;
                Log.v("App", message.toString());
                message.send(gcm);
            }
        }
    }
}