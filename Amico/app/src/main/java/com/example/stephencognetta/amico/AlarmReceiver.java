package com.example.stephencognetta.amico;

/**
 * Created by stephencognetta on 4/16/15.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Calendar c = Calendar.getInstance();
        Log.v("Alarm", "alarming with " + SharedPrefs.timeSinceSent + " vs " + c.getTimeInMillis());
        if (SharedPrefs.timeSinceSent != -1 && (SharedPrefs.timeSinceSent + 1000*60*60 < c.getTimeInMillis())) {
            Log.w("Notification", "We gotta cancel our notifications folks");
            android.app.NotificationManager mNotifyMgr =
                    (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotifyMgr.cancelAll();
            SharedPrefs.timeSinceSent = c.getTimeInMillis();

            // For our recurring task, we'll just display a message
        }
    }
}