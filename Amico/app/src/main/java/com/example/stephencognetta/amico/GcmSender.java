package com.example.stephencognetta.amico;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * private functions used for sending messages
 */
public class GcmSender {
    public static final String PROJECT_NUMBER = "596039776570";
    public static AtomicInteger msgId = new AtomicInteger();

    static final String APPLICATION = "application";
    static final String TYPE = "type";
    static final String ACTION = "action";
    static final String MESSAGETYPE = "messagetype";

    static final String DEVICETYPE = "devicetype";
    static final String PIN = "pin";

    static final String DEVICEID = "deviceid";

    public static class PairingMessage {
        static final String messageType = "pairing";
        static final String devicetype = "android";
        static final String action = "finish";
        String secretcode;
        public PairingMessage (String secretcode) {
            this.secretcode = secretcode;
        }
        public void send(GoogleCloudMessaging gcm) {
            Log.v("GCM", "Sending Pairing Message");
            if (secretcode == null) {
                throw new NullPointerException("Pairing Message fields not all set.");
            }
            HashMap<String, String> data = new HashMap<String, String>();
            data.put(MESSAGETYPE, messageType);
            data.put(ACTION, action);
            data.put(DEVICETYPE, devicetype);
            data.put(PIN, secretcode);
            sendMessage(gcm, data);
        }
    }

    public static class RefreshMessage {
        static final String messageType = "refresh";
        public void send(GoogleCloudMessaging gcm) {
            Log.v("GCM", "Sending Refresh Message");
            HashMap<String, String> data = new HashMap<String, String>();
            data.put(MESSAGETYPE, messageType);
            sendMessage(gcm, data);
        }
    }

    public static class ActionMessage {
        static final String messageType = "browser_action";
        String application;
        String type;
        String action;
        String deviceid;

        @Override
        public String toString() {
            return application + " " +  type + " " + action;
        }

        public void send(GoogleCloudMessaging gcm) {
            Log.v("GCM", "Sending Action Message");
            if (application == null || type == null || action == null) {
                throw new NullPointerException("Action Message fields not all set.");
            }
            HashMap<String, String> data = new HashMap<String, String>();
            data.put(MESSAGETYPE, messageType);
            data.put(TYPE, type);
            data.put(APPLICATION, application);
            data.put(ACTION, action);
            data.put(DEVICEID, deviceid);
            sendMessage(gcm, data);
        }
    }

    private static void sendMessage(
            final GoogleCloudMessaging gcm,
            final Map<String, String> parameters) {


        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    String msg = "";
                    Bundle data = new Bundle();
                    for (Map.Entry<String, String> param : parameters.entrySet()) {
                        data.putString(param.getKey(), param.getValue());
                        msg += "\n" + param.getKey() + ": " + param.getValue();
                    }
                    String id = Integer.toString(msgId.incrementAndGet());

                    gcm.send(PROJECT_NUMBER + "@gcm.googleapis.com", id, data);

                    Log.v("GCM", "Sent message" + msg);


                } catch (IOException ex) {
                    Log.v("GCM", "Error :" + ex.getMessage());
                }
                return null;
            }
            @Override protected void onPostExecute(Void msg) {}
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
