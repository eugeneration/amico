package com.example.stephencognetta.amico;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.Map;

public class SharedPrefs {
    public static long timeSinceSent;

    // a cheat to allow the MainActivity to know what happened since it was last paused
    public static Map<String, Bundle> updateQueue = null;

    // Shared settings file for registration related things
    public static final String PREFS_REG = "RegistrationPrefsFile";

    // String. Shared preference for storing this device's regid
    public static final String REGID_PREF = "regId";

    // String. Shared preference for storing this device's deviceid
    public static final String DEVICEID_PREF = "deviceId";

    // Boolean. Shared preference stating if this device has successfully paired with another device.
    public static final String PAIRED_PREF = "paired";

    // returns true is this device has been paired
    public static boolean deviceIsPaired(Context context) {
        SharedPreferences settings = getSharedPreferences(context);
        return settings.contains(REGID_PREF)
                && settings.contains(DEVICEID_PREF)
                && settings.contains(PAIRED_PREF);
    }

    public static String getDeviceId(Context context) {
        SharedPreferences settings = getSharedPreferences(context);
        return settings.getString(DEVICEID_PREF, "");
    }

    public static String getRegId(Context context) {
        SharedPreferences settings = getSharedPreferences(context);
        return settings.getString(REGID_PREF, "");
    }

    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SharedPrefs.PREFS_REG, 0);
    }
}
