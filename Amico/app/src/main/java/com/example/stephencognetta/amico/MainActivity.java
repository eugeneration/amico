package com.example.stephencognetta.amico;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.support.v4.app.FragmentActivity;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.example.messages.ActionMessage;
import com.example.messages.RefreshMessage;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.example.stephencognetta.image.DrawActivity;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.wearable.Wearable;

import java.io.File;
import java.io.InputStream;
import java.sql.Time;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardView;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transfermanager.*;
import com.amazonaws.regions.Regions;

public class MainActivity extends FragmentActivity {
    private static Context context;
    //private static Toast toast;
    private boolean isGoogleSlides;
    public static GoogleApiClient mApiClient;
    private static MenuItem waitingItem;
    public static final String YELP = "yelp";
    public static final Application EIGHTTRACKS = new Application("eighttracks", "8Tracks", 001);
    public static final Application PANDORA = new Application("pandora", "Pandora", 002);
    public static final Application YOUTUBE = new Application("youtube", "YouTube", 003);
    public static final Application VIMEO = new Application("vimeo", "Vimeo", 004);
    public static final Application PREZI = new Application("prezi", "Prezi", 005);
    public static final Application SPOTIFY = new Application("spotify", "Spotify", 006);
    public static final Application NETFLIX = new Application("netflix", "Netflix", 8);
    public static final Application IHEARTRADIO = new Application("iheartradio", "I Heart Radio", 9);
    public static final Application SLIDESHARE = new Application("slideshare", "Slideshare", 10);
    public static final Application SONGZA = new Application("songza", "Songza", 11);
    public static final Application SOUNDCLOUD = new Application("soundcloud", "Soundcloud", 12);
    public static final Application GROOVESHARK = new Application("grooveshark", "Grooveshark", 13);
    public static final Application GOOGLESLIDES = new Application("googleslides", "Google Slides", 14);
    public static final Application DOCS = new Application("docs", "Google Docs", 14);
    private PendingIntent pendingIntent;

    //This is a step toward making the code more modular. This should be expanded to further
    //the goal of modularizing.
    public static class Application {
        public String nameAbbrev;
        public String nameFull;
        public int notificationId;
        public Application(String nameAbbrev, String nameFull, int notificationId) {
            this.nameAbbrev = nameAbbrev;
            this.nameFull = nameFull;
            this.notificationId = notificationId;
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        if (!isGoogleSlides) {
            return super.dispatchKeyEvent(event);
        }
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN) {
                    Log.v("Yooo", "Up");
                    onClickGoogleSlides(findViewById(R.id.nextGoogleSlides));
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN) {
                    Log.v("Yooo", "Down");
                    onClickGoogleSlides(findViewById(R.id.previousGoogleSlides));
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }

    public static String PACKAGE_NAME;
    public int cardCount;

    private GoogleMap mMap;
    Button btnRegId;

    static GoogleCloudMessaging gcm;

    // S3 variables
    private static int SELECT_PICTURE = 100;
    private static int DRAW = 101;
    private TransferManager transferManager;

    // a cheat to get this activity context from anywhere when it is active.
    public static MainActivity activity;

    @Override
    protected void onStart() {
        super.onStart();

        if (!SharedPrefs.deviceIsPaired(this)) {
            Log.v("registration", "Needs to register or pair - going to registration activity.");
            goToRegistrationActivity();
        }
    }

    private void initGoogleApiClient(Context context) {
        Log.v("Yooo", "creating my guy");
        mApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        Log.d("Yoo", "onConnected: " + connectionHint);
                        // Now you can use the Data Layer API
                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                        Log.d("Yoo", "onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Log.d("Yoo", "onConnectionFailed: " + result);
                    }
                })
                        // Request access only to the Wearable API
                .addApi(Wearable.API)
                .build();

        mApiClient.connect();
        Log.v("Yooo", "connecting my guy");


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cardCount = targetCardPosition.size();
        Log.v("Notification", "Oncreate");
        isGoogleSlides = false;
        setContentView(R.layout.activity_main);
        PACKAGE_NAME = getApplicationContext().getPackageName();
        MainActivity.context = getApplicationContext();

        gcm = GoogleCloudMessaging.getInstance(this);

        // S3
        CognitoCachingCredentialsProvider cognitoProvider = new CognitoCachingCredentialsProvider(
                this, "us-east-1:54aafb26-d7f3-4067-a001-3349971422dd", Regions.US_EAST_1);
        transferManager = new TransferManager(cognitoProvider);

        //Store activity
        activity = this;

        setAlarm(getApplicationContext());
        initGoogleApiClient(this);

        // hacky way to get around storing the queue in a file for when app is updated
        if (SharedPrefs.updateQueue == null) {
            SharedPrefs.updateQueue = new HashMap<String, Bundle>();
            // ask the server for a full update
            new RefreshMessage(this).send();
        }
        else {
            // fill in the cards stored in memory
            for (Bundle cardInfo : SharedPrefs.updateQueue.values()) {
                addCard(cardInfo);
            }
        }
        checkIfNoCards();
        SharedPrefs.timeSinceSent = -1;
        new CheckNotification().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public static void setAlarm(Context context) {
        /* Retrieve a PendingIntent that will perform a broadcast */
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        int interval = 30*60*1000; //30 minutes

        manager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), interval, pi);
    }

    public static Context getAppContext() {
        return MainActivity.context;
    }

    public void sendRefresh() {
        new RefreshMessage(this).send();
    }
    @Override
    protected void onPause() {
        super.onPause();
        activity = null;
    }
    @Override
    protected void onResume() {
        super.onResume();
        activity = this;

        // if stuff happened while android was sleeping, do them
        if (!SharedPrefs.updateQueue.isEmpty()) {
            Log.v("GCM", "Here are some messages from when you were sleeping:");
            for (String application : SharedPrefs.updateQueue.keySet()){
                Bundle extras = SharedPrefs.updateQueue.get(application);
                if (extras.getString("action").equals("add")) {
                    Log.v("GCM", "I'm being instructed to Add a Card");
                    activity.addCard(extras);
                } else if (extras.getString("action").equals("remove")) {
                    Log.v("GCM", "I'm being instructed to Remove a Card");
                    activity.removeCard(extras);
                } else if (extras.getString("action").equals("update")) {
                    Log.v("GCM", "I'm being instructed to update a Card");
                    activity.updateCard(extras);
                }
            }
            SharedPrefs.updateQueue.clear();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        waitingItem = menu.findItem(R.id.waitingIcon);
        waitingItem.setVisible(false);
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                // Upload this selected image to S3
                Uri selectedImageUri = data.getData();
                String filemanagerstring = selectedImageUri.getPath(); //OI FILE Manager
                String selectedImagePath = getPath(selectedImageUri); //MEDIA GALLERY

                Log.v("file", selectedImagePath + " " + filemanagerstring);

                String file;
                if (selectedImagePath != null)
                    file = selectedImagePath;
                else
                    file = filemanagerstring;

                String filename = SharedPrefs.getRegId(this) + System.currentTimeMillis();
                transferManager.upload(
                        new PutObjectRequest("pairit", "img/" + filename, new java.io.File(file))
                                .withCannedAcl(CannedAccessControlList.PublicRead));

                new ActionMessage(this, DOCS.nameAbbrev, "place_image")
                        .addParam("image_src", "https://s3.amazonaws.com/pairit/img/" + filename)
                        .send();
                MainActivity.handleToast(0);
            }
            if (requestCode == DRAW) {
                File root = Environment.getExternalStorageDirectory();
                File file = new File(root, "pairit_drawing_temp.png");

                String filename = SharedPrefs.getRegId(this) + System.currentTimeMillis();
                transferManager.upload(
                        new PutObjectRequest("pairit", "img/" + filename, file)
                                .withCannedAcl(CannedAccessControlList.PublicRead));
                new ActionMessage(this, DOCS.nameAbbrev, "place_image")
                        .addParam("image_src", "https://s3.amazonaws.com/pairit/img/" + filename)
                        .send();
                MainActivity.handleToast(0);
            }
        }
    }
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if(cursor != null) {
            //HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            //THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        else return null;
    }

    public static void handleToast(int kill ) {
        Log.v("Toast", "Getting toasty in here");
        if (kill == 0) {
            waitingItem.setVisible(true);
            //get time here
            Calendar c = Calendar.getInstance();
        }
        else {
            waitingItem.setVisible(false);
            //reset time here
        }
    }
     //failed attempt to kill notifications
    public class CheckNotification extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            while (true) {
                int i = 0;
                /*Calendar c = Calendar.getInstance();
                if (SharedPrefs.timeSinceSent != -1 && (SharedPrefs.timeSinceSent + 15 < c.get(Calendar.SECOND))) {
                    Log.v("Notification", "We gotta cancel our notifications folks");
                    android.app.NotificationManager mNotifyMgr =
                            (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotifyMgr.cancelAll();
                    SharedPrefs.timeSinceSent = c.get(Calendar.SECOND);
                }*/
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.refresh:
                sendRefresh();
                MainActivity.handleToast(0);
                return true;
            case R.id.action_feedback:
                sendFeedback();
                return true;
            case R.id.settings_menu:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // some variables to keep track of card positions
    private List<String> targetCardPosition = new ArrayList<String>();
    public void addCard(Bundle extras) {

        String application = extras.getString("application");

        // only add the card if it doesn't already exist
        if (targetCardPosition.indexOf(application) == -1) {
            String title = extras.getString("title");
            if (application.equals(EIGHTTRACKS.nameAbbrev)) {
                createCard(R.layout.row_card_8tracks, "8Tracks", title);

            } else if (application.equals(VIMEO.nameAbbrev)) {
                createCard(R.layout.row_card_vimeo, "Vimeo", title);
            } else if (application.equals(PANDORA.nameAbbrev)) {
                createCard(R.layout.row_card_pandora, "Pandora", title);
            } else if (application.equals(PREZI.nameAbbrev)) {
                createCard(R.layout.row_card_prezi, "Prezi", title);
            } else if (application.equals(SPOTIFY.nameAbbrev)) {
                createCard(R.layout.row_card_spotify, "Spotify", title);
            } else if (application.equals(YOUTUBE.nameAbbrev)) {
                createCard(R.layout.row_card_youtube, "Youtube", title);
            } else if (application.equals(NETFLIX.nameAbbrev)) {
                createCard(R.layout.row_card_netflix, "Netflix", title);
            } else if (application.equals(IHEARTRADIO.nameAbbrev)) {
                createCard(R.layout.row_card_iheartradio, "I Heart Radio", title);
            } else if (application.equals(SLIDESHARE.nameAbbrev)) {
                createCard(R.layout.row_card_slideshare, "Slideshare", title);
            } else if (application.equals(GROOVESHARK.nameAbbrev)) {
                createCard(R.layout.row_card_grooveshark, "Grooveshark", title);
            } else if (application.equals(SONGZA.nameAbbrev)) {
                createCard(R.layout.row_card_songza, "Songza", title);
            } else if (application.equals(SOUNDCLOUD.nameAbbrev)) {
                createCard(R.layout.row_card_soundcloud, "Soundcloud", title);
            } else if (application.equals(GOOGLESLIDES.nameAbbrev)) {
                isGoogleSlides = true;
                createCard(R.layout.row_card_googleslides, "Google Slides", title);
            } else if (application.equals(YELP)) {
                String latlng = extras.getString("latlng");
                createMapCard(R.layout.row_card_map, "Yelp", title, latlng);
            } else if (application.equals(DOCS.nameAbbrev)) {
                createCard(R.layout.row_card_docs, DOCS.nameFull, title);
            } else {
                Log.v("GCM", "Target unknown.");
                return;
            }
            targetCardPosition.add(application);
        }
    }
    public void removeCard(Bundle extras){
        String application = extras.getString("application");
        // remove cards
        Log.v("GCM", "removing a card");
        LinearLayout parent = (LinearLayout) findViewById(R.id.cardContainer);
        int cardIndex = targetCardPosition.indexOf(application);

        if (cardIndex != -1) {
            parent.removeViewAt(cardIndex);
            targetCardPosition.remove(cardIndex);
        }
        if (application.equals(GOOGLESLIDES.nameAbbrev)) {
            isGoogleSlides = false;
        }
        cardCount--;
        checkIfNoCards();
    }

    public void updateCard(Bundle extras){
        String application = extras.getString("application");
        // remove cards
        Log.v("GCM", "updating a card");
        LinearLayout parent = (LinearLayout) findViewById(R.id.cardContainer);
        int cardIndex = targetCardPosition.indexOf(application);

        if (cardIndex != -1) {
            parent.removeViewAt(cardIndex);
            targetCardPosition.remove(cardIndex);
            String title = extras.getString("title");
            if (application.equals(EIGHTTRACKS.nameAbbrev)) {
                createCard(R.layout.row_card_8tracks, "8Tracks", title);
            } else if (application.equals(VIMEO.nameAbbrev)) {
                createCard(R.layout.row_card_vimeo, "Vimeo", title);
            } else if (application.equals(PANDORA.nameAbbrev)) {
                createCard(R.layout.row_card_pandora, "Pandora", title);
            } else if (application.equals(SPOTIFY.nameAbbrev)) {
                createCard(R.layout.row_card_spotify, "Spotify", title);
            } else if (application.equals(NETFLIX.nameAbbrev)) {
                createCard(R.layout.row_card_netflix, "Netflix", title);
            } else if (application.equals(GROOVESHARK.nameAbbrev)) {
                createCard(R.layout.row_card_grooveshark, "Grooveshark", title);
            } else if (application.equals(GOOGLESLIDES.nameAbbrev)) {
                isGoogleSlides = true;
                createCard(R.layout.row_card_googleslides, "Google Slides", title);
            } else if (application.equals(IHEARTRADIO.nameAbbrev)) {
                createCard(R.layout.row_card_iheartradio, "I Heart Radio", title);
            } else if (application.equals(SLIDESHARE.nameAbbrev)) {
                createCard(R.layout.row_card_slideshare, "Slideshare", title);
            } else if (application.equals(SONGZA.nameAbbrev)) {
                createCard(R.layout.row_card_songza, "Songza", title);
            } else if (application.equals(SOUNDCLOUD.nameAbbrev)) {
                createCard(R.layout.row_card_soundcloud, "Soundcloud", title);
            } else if (application.equals(PREZI.nameAbbrev)) {
                createCard(R.layout.row_card_prezi, "Prezi", title);
            } else if (application.equals(YOUTUBE.nameAbbrev)) {
                createCard(R.layout.row_card_youtube, "Youtube", title);
            } else if (application.equals(YELP)) {
                String latlng = extras.getString("latlng");
                createMapCard(R.layout.row_card_map, "Yelp", title, latlng);
            } else if (application.equals(DOCS.nameAbbrev)) {
                    createCard(R.layout.row_card_docs, DOCS.nameFull, title);
            } else {
                Log.v("GCM", "Target unknown.");
                return;
            }
            targetCardPosition.add(application);
        }
        else {
            String title = extras.getString("title");
            if (application.equals(EIGHTTRACKS.nameAbbrev)) {
                createCard(R.layout.row_card_8tracks, "8Tracks", title);
            } else if (application.equals(VIMEO.nameAbbrev)) {
                createCard(R.layout.row_card_vimeo, "Vimeo", title);
            } else if (application.equals(PANDORA.nameAbbrev)) {
                createCard(R.layout.row_card_pandora, "Pandora", title);
            } else if (application.equals(PREZI.nameAbbrev)) {
                createCard(R.layout.row_card_prezi, "Prezi", title);
            } else if (application.equals(SPOTIFY.nameAbbrev)) {
                createCard(R.layout.row_card_spotify, "Spotify", title);
            } else if (application.equals(NETFLIX.nameAbbrev)) {
                createCard(R.layout.row_card_netflix, "Netflix", title);
            } else if (application.equals(GROOVESHARK.nameAbbrev)) {
                createCard(R.layout.row_card_grooveshark, "Grooveshark", title);
            } else if (application.equals(GOOGLESLIDES.nameAbbrev)) {
                isGoogleSlides = true;
                createCard(R.layout.row_card_googleslides, "Google Slides", title);
            } else if (application.equals(IHEARTRADIO.nameAbbrev)) {
                createCard(R.layout.row_card_iheartradio, "I Heart Radio", title);
            } else if (application.equals(SLIDESHARE.nameAbbrev)) {
                createCard(R.layout.row_card_slideshare, "Slideshare", title);
            } else if (application.equals(SONGZA.nameAbbrev)) {
                createCard(R.layout.row_card_songza, "Songza", title);
            } else if (application.equals(SOUNDCLOUD.nameAbbrev)) {
                createCard(R.layout.row_card_soundcloud, "Soundcloud", title);
            } else if (application.equals(YOUTUBE.nameAbbrev)) {
                createCard(R.layout.row_card_youtube, "Youtube", title);
            } else if (application.equals(YELP)) {
                String latlng = extras.getString("latlng");
                createMapCard(R.layout.row_card_map, "Yelp", title, latlng);
            } else if (application.equals(DOCS.nameAbbrev)) {
                createCard(R.layout.row_card_docs, DOCS.nameFull, title);
            } else {
                Log.v("GCM", "Target unknown.");
                return;
            }
            targetCardPosition.add(application);
        }
    }



    private CardView createCard(int type, String title, String cardTitle) {

        // parent is the card linear layout
        LinearLayout parent = (LinearLayout) findViewById(R.id.cardContainer);

        Card card = new Card(this, type);

        // Create a CardHeader
        CardHeader header = new CardHeader(this);
        header.setTitle(title);

        card.setTitle(cardTitle);
//        only on info cards
//        card.setSwipeable(true);
//        card.setOnSwipeListener(new Card.OnSwipeListener() {
//            @Override
//            public void onSwipe(Card card) {}
//        });

//        CardThumbnail thumb = new CardThumbnail(this);
        //thumb.setDrawableResource(R.drawable.ic_launcher);
//
//        card.addCardThumbnail(thumb);


        // Add Header to card
        card.addCardHeader(header);

        // Inflate new card
        CardView cardView = (CardView) LayoutInflater.from(this).inflate(R.layout.row_card, parent, false);
        cardView.setCard(card);

        // add to List
        if (targetCardPosition.size() == 0) {
            parent.removeAllViews();
        }
        parent.addView(cardView);

        cardCount = targetCardPosition.size();
        cardCount++;
        return cardView;
    }

    private void createEmptyCard () {
        // parent is the card linear layout
        LinearLayout parent = (LinearLayout) findViewById(R.id.cardContainer);
        View cardView = LayoutInflater.from(this).inflate(R.layout.row_card_empty, parent, false);
        parent.addView(cardView);
    }

    private void createMapCard(int type, String title, String cardTitle, String latlng) {

        CardView cardView = createCard(type, title, cardTitle);

        String url = "http://maps.google.com/maps/api/staticmap?center=" + latlng
                + "&zoom=15&size=1200x400&sensor=false&markers=color:red|label:C|" + latlng;
        ImageView map = (ImageView) cardView.findViewById(R.id.card_main_static_map);
        new DownloadImageTask(map).execute(url);

//      Map intent
//       Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                Uri.parse("http://maps.google.com/maps?daddr=" + latlng));
//        startActivity(intent);
    }

    public void onClickPrezi(View v) {
        String action;
        if (v == findViewById(R.id.nextPrezi)) {
            action = "next";
        } else if (v == findViewById(R.id.previousPrezi)){
            action = "previous";
        } else {
            return;
        }
        new ActionMessage(this, PREZI.nameAbbrev, action).send();
        MainActivity.handleToast(0);
    }
    public void onClickVimeo(View v) {
        String action;

        if (v == findViewById(R.id.playVimeo)) {
            action = "play";
        } else if (v == findViewById(R.id.pauseVimeo)){
            action = "pause";
        } else {
            return;
        }
        new ActionMessage(this, VIMEO.nameAbbrev, action).send();
        MainActivity.handleToast(0);

    }
    public void onClick8Tracks(View v) {
        String action;
        if (v == findViewById(R.id.play8Tracks)) {
            action = "play";
        } else if (v == findViewById(R.id.pause8Tracks)){
            action = "pause";
        } else if (v == findViewById(R.id.skip8Tracks)) {
            action = "skip";
        } else {
            return;
        }
        new ActionMessage(this, EIGHTTRACKS.nameAbbrev, action).send();
        MainActivity.handleToast(0);

    }
    public void onClickPandora(View v) {
        String action;

        if (v == findViewById(R.id.playPandora)) {
            action = "play";
        } else if (v == findViewById(R.id.pausePandora)){
            action = "pause";
        } else if (v == findViewById(R.id.skipPandora)) {
            action = "skip";
        } else {
            return;
        }
        new ActionMessage(this, PANDORA.nameAbbrev, action).send();
        MainActivity.handleToast(0);

    }
    public void onClickNetflix(View v) {
        String action;
        if (v == findViewById(R.id.playNetflix)) {
            action = "play";
        } else if (v == findViewById(R.id.pauseNetflix)) {
            action = "pause";
        } else {
            return;
        }
        new ActionMessage(this, NETFLIX.nameAbbrev, action).send();
        MainActivity.handleToast(0);

    }
    public void onClickGrooveshark(View v) {
        String action;
        if (v == findViewById(R.id.playGrooveshark)) {
            action = "play";
        } else if (v == findViewById(R.id.pauseGrooveshark)){
            action = "pause";
        } else if (v == findViewById(R.id.skipGrooveshark)) {
            action = "skip";
        } else {
            return;
        }
        new ActionMessage(this, GROOVESHARK.nameAbbrev, action).send();
        MainActivity.handleToast(0);

    }
    public void onClickIHeartRadio(View v) {
        String action;
        if (v == findViewById(R.id.playIHeartRadio)) {
            action = "play";
        } else if (v == findViewById(R.id.pauseIHeartRadio)){
            action = "pause";
        } else {
            return;
        }
        new ActionMessage(this, IHEARTRADIO.nameAbbrev, action).send();
        MainActivity.handleToast(0);

    }
    public void onClickSlideshare(View v) {
        String action;
        if (v == findViewById(R.id.nextSlideshare)) {
            action = "next";
        } else if (v == findViewById(R.id.previousSlideshare)){
            action = "previous";
        } else {
            return;
        }
        new ActionMessage(this, SLIDESHARE.nameAbbrev, action).send();
        MainActivity.handleToast(0);

    }
    public void onClickGoogleSlides(View v) {
        String action;
        if (v == findViewById(R.id.nextGoogleSlides)) {
            action = "next";
        } else if (v == findViewById(R.id.previousGoogleSlides)){
            action = "previous";
        } else {
            return;
        }
        new ActionMessage(this, GOOGLESLIDES.nameAbbrev, action).send();
        MainActivity.handleToast(0);

    }
    public void onClickSongza(View v) {
        String action;
        if (v == findViewById(R.id.playSongza)) {
            action = "play";
        } else if (v == findViewById(R.id.pauseSongza)){
            action = "pause";
        } else if (v == findViewById(R.id.skipSongza)) {
            action = "skip";
        } else {
            return;
        }
        new ActionMessage(this, SONGZA.nameAbbrev, action).send();
        MainActivity.handleToast(0);

    }
    public void onClickSoundcloud(View v) {
        String action;
        if (v == findViewById(R.id.playSoundcloud)) {
            action = "play";
        } else if (v == findViewById(R.id.pauseSoundcloud)){
            action = "pause";
        } else if (v == findViewById(R.id.skipSoundcloud)) {
            action = "skip";
        } else {
            return;
        }
        new ActionMessage(this, SOUNDCLOUD.nameAbbrev, action).send();
        MainActivity.handleToast(0);

    }
    public void onClickSpotify(View v) {
        String action;
        if (v == findViewById(R.id.playSpotify)) {
            action = "play";
        } else if (v == findViewById(R.id.pauseSpotify)){
            action = "pause";
        } else if (v == findViewById(R.id.skipSpotify)) {
            action = "skip";
        } else {
            return;
        }
        new ActionMessage(this, SPOTIFY.nameAbbrev, action).send();
        MainActivity.handleToast(0);

    }
    public void onClickYoutube(View v) {
        String action;
        if (v == findViewById(R.id.playYouTube)) {
            action = "play";
        } else if (v == findViewById(R.id.pauseYouTube)){
            action = "pause";
        } else {
            return;
        }
        new ActionMessage(this, YOUTUBE.nameAbbrev, action).send();
        MainActivity.handleToast(0);
    }
    public void onClickDocs(View v) {
        if (v == findViewById(R.id.imageDocs)) {
            // choose an image from gallery
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
        } else if ( v == findViewById(R.id.drawDocs)) {
            // draw something
            Intent intent = new Intent(this, DrawActivity.class);
            startActivityForResult(intent, DRAW);
        } else {
            return;
        }
        MainActivity.handleToast(0);
    }

    //On click event for button sendFeedback
    public void sendFeedback() {
        final Intent _Intent = new Intent(android.content.Intent.ACTION_SEND);
        _Intent.setType("text/html");
        _Intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ getString(R.string.mail_feedback_email) });
        _Intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.mail_feedback_subject));
        _Intent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.mail_feedback_message));
        startActivity(Intent.createChooser(_Intent, getString(R.string.title_send_feedback)));
    }

    // from http://stackoverflow.com/questions/2471935/how-to-load-an-imageview-by-url-in-android/9288544#9288544
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
    private void checkIfNoCards() {
        Log.v("cardcount", "number of cards: " + targetCardPosition.size());
        if (targetCardPosition.size() == 0) {
            createEmptyCard();
        }
    }

    private void goToRegistrationActivity() {
        Intent intent = new Intent(this, PairingActivity.class);
        startActivity(intent);
        finish();
    }
}
