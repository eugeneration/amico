package com.example.stephencognetta.amico;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.example.messages.GcmMessage;
import com.example.messages.PairingMessage;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

public class PairingActivity extends Activity {
    // TODO: use the recommended method as seen on stack overflow
    public static PairingActivity activity;

    // TODO: According to docs, existing regid might not work when app is updated
    // https://developer.android.com/google/gcm/client.html
    SharedPreferences settings;

    @Override
    protected void onStart () {
        super.onStart();

        // if already registered, successful registration
        settings = SharedPrefs.getSharedPreferences(this);
        if (SharedPrefs.deviceIsPaired(this)) {
            Log.v("registration", "Already registered...going back to MainActivity.");
            goToMainActivity();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        Log.v("registration", "Starting registration.");

        activity = this;
        getRegId();

        EditText editText = (EditText) findViewById(R.id.edit_message);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    attemptPair();
                    handled = true;
                }
                return handled;
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        activity = null;
    }
    @Override
    protected void onResume() {
        super.onResume();
        activity = this;
    }

    public void getRegId(){
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg;
                try {
                    GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    String regid = gcm.register(GcmMessage.PROJECT_NUMBER);

                    settings.edit().putString(SharedPrefs.REGID_PREF, regid).apply();
                    msg = "Device registered, registration ID=" + regid;
                    Log.i("GCM", msg);

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);
    }

    public void onClickPairing (View v) {
        attemptPair();
    }

    public void attemptPair () {
        ProgressDialog progress;

        progress = new ProgressDialog(this);
        progress.setTitle("Connecting Device");
        progress.setMessage("Please wait while we try to connect your device. If this screen takes over 30 seconds, " +
                "please double check the code and enter it again.");
        progress.setCancelable(true);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();

        // Only works if regid is set -
        if (SharedPrefs.getRegId(this).isEmpty()) {
            Log.v("registration", "Device hasn't been registered with GCM yet. Try again.");
            return;
        }
        //send registration over to server
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String pairingPIN = editText.getText().toString();
        new PairingMessage(this, pairingPIN).send();
    }

    public void pairingSuccessful(String deviceid) {
        // must be commit - otherwise, will go to Main Activity before the value changes
        settings.edit().putBoolean(SharedPrefs.PAIRED_PREF, true).commit();
        settings.edit().putString(SharedPrefs.DEVICEID_PREF, deviceid).commit();
        goToMainActivity();
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
