package com.example.stephencognetta.amico;

import android.app.ActionBar;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
    private static final String START_ACTIVITY = "/start_activity";

    public static AtomicInteger piId = new AtomicInteger();
    private void setRemoteView(RemoteViews.RemoteView rv) {

    }




    private void sendMessage( final String path, final String text ) {
        Log.i("yooo", "messaaaaage");
        if(MainActivity.mApiClient.isConnected()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.d("Yooo", "Sending message like i said");
                    NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(MainActivity.mApiClient).await();
                    for (Node node : nodes.getNodes()) {
                        Log.i("yoooo", node.getDisplayName());
                        Wearable.MessageApi.sendMessage(
                                MainActivity.mApiClient, node.getId(), path, text.getBytes()).setResultCallback(
                                new ResultCallback<MessageApi.SendMessageResult>() {
                                    @Override
                                    public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                        if (!sendMessageResult.getStatus().isSuccess()) {
                                            Log.e("Yoo", "Failed to send message with status code: "
                                                    + sendMessageResult.getStatus().getStatusCode());
                                        } else {
                                            Log.i("yoooo", "success!!: " );
                                        }
                                    }
                                }
                        );
                    }

                }
            }).start();
        }
    }



    @Override
    public void onReceive(Context context, Intent intent) {
        Log.w("Yo", "We've received something!");
        Calendar c = Calendar.getInstance();
        SharedPrefs.timeSinceSent = c.getTimeInMillis();

//        // Explicitly specify that GcmMessageHandler will handle the intent.
//        ComponentName comp = new ComponentName(context.getPackageName(),
//                GcmMessageHandler.class.getName());
//
//        // Start the service, keeping the device awake while it is launching.
//        startWakefulService(context, (intent.setComponent(comp)));
//        setResultCode(Activity.RESULT_OK);


        Bundle extras = intent.getExtras();

        if (extras == null) {
            Log.v("Yo", "no extras... :(");
        } else {
            String messageString = "";
            for (String key : extras.keySet()) {
                messageString += "\n" + key + ":" + extras.get(key);
            }
            Log.v("Application Name", "Message received:" + messageString);

            if(extras.getString("message_type", "").equals("send_event")) {
               Log.v("Ack", "Received an acknowledgement of our send!");
                // kill the refreshing thing
                MainActivity.handleToast(1);

            }
            /***************MAKE CARD****************/


            // get the update message
            String messagetype = extras.getString("messagetype", "");
            if (!messagetype.isEmpty()) {
                String deviceid = extras.getString("deviceid", "");

                /***************SUCCESSFUL REGISTRATION****************/
                if (messagetype.equals("pairing")) {
                    // check if the reg activity is open and active (not paused)
                    // TODO: shouldn't be dependent on the activity being open
                    if (PairingActivity.activity != null) {
                        Log.v("GCM", "Successful Pairing!");
                        PairingActivity.activity.pairingSuccessful(deviceid);
                    }
                }
                else {
                    // device id must exist and match to continue
                    String myDeviceid = SharedPrefs.getDeviceId(context);
                    if (myDeviceid.isEmpty()) {
                        Log.v("GCM", "Device id not set!");
                    } else if (deviceid.isEmpty()) {
                        Log.v("GCM", "No Device id in message");
                    } else if (!myDeviceid.equals(deviceid)) {
                        Log.v("GCM", "Device Id Mismatch!");
                        // TODO: send message to server to remove this device id
                    } else {
                        /**************CARD RELATED MESSAGES****************/
                        if (messagetype.equals("android_card")) {
                            // check if the main activity is open and active (not paused)
                            if (MainActivity.activity != null) {
                                if (extras.getString("action").equals("add")) {
                                    Log.v("GCM", "I'm being instructed to Add a Card");
                                    MainActivity.activity.addCard(extras);
                                } else if (extras.getString("action").equals("remove")) {
                                    Log.v("GCM", "I'm being instructed to Remove a Card");
                                    MainActivity.activity.removeCard(extras);
                                } else if (extras.getString("action").equals("update")) {
                                    Log.v("GCM", "I'm being instructed to update a Card");
                                    MainActivity.activity.updateCard(extras);
                                }
                            }

                            // store the current card state
                            Log.v("GCM", "Storing some info");
                            String application = extras.getString("application", "");
                            // hacky way to get around storing the queue in a file
                            if (SharedPrefs.updateQueue == null) {
                                SharedPrefs.updateQueue = new HashMap<String, Bundle>();
                            }
                            SharedPrefs.updateQueue.put(application, extras);
                        }
                    }
                }
            }

            /***************MAKE NOTIFICATION****************/
            //check if notifications are enabled
            SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);

            boolean blockNotifications = SP.getBoolean("notifications",false);
            if (blockNotifications == true) {
                Log.w("Notification", "notifications are blocked");
                // Gets an instance of the NotificationManager service
                android.app.NotificationManager mNotifyMgr =
                        (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotifyMgr.cancelAll();
                return;
            }
            else {
                Log.w("Notification", "notifications are not blocked.");

            }

            Log.v("Notification", "I'm about to make a notification");
            if (extras.containsKey(("action")) && extras.get("action").equals("update")) {
                sendMessage( START_ACTIVITY, extras.toString());



                Intent playIntent = new Intent(context, NotifyActivityHandler.class);
                playIntent.putExtra("methodName", "" + extras.get("application"));
                playIntent.putExtra("action", "" + "play");
                playIntent.putExtra("deviceid", "" + SharedPrefs.getDeviceId(context));
                android.app.PendingIntent piPlay =
                        android.app.PendingIntent.getService(
                                context,
                                piId.getAndIncrement(),
                                playIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                Intent pauseIntent = new Intent(context, NotifyActivityHandler.class);
                pauseIntent.putExtra("methodName", "" + extras.get("application"));
                pauseIntent.putExtra("action", "" + "pause");
                pauseIntent.putExtra("deviceid", "" + SharedPrefs.getDeviceId(context));
                android.app.PendingIntent piPause =
                        android.app.PendingIntent.getService(
                                context,
                                piId.getAndIncrement(),
                                pauseIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                Intent nextIntent = new Intent(context, NotifyActivityHandler.class);
                nextIntent.putExtra("methodName", "" + extras.get("application"));
                nextIntent.putExtra("action", "" + "next");
                nextIntent.putExtra("deviceid", "" + SharedPrefs.getDeviceId(context));
                android.app.PendingIntent piNext =
                        android.app.PendingIntent.getService(
                                context,
                                piId.getAndIncrement(),
                                nextIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                Intent skipIntent = new Intent(context, NotifyActivityHandler.class);
                skipIntent.putExtra("methodName", "" + extras.get("application"));
                skipIntent.putExtra("action", "" + "skip");
                skipIntent.putExtra("deviceid", "" + SharedPrefs.getDeviceId(context));
                android.app.PendingIntent piSkip =
                        android.app.PendingIntent.getService(
                                context,
                                piId.getAndIncrement(),
                                skipIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                Intent previousIntent = new Intent(context, NotifyActivityHandler.class);
                previousIntent.putExtra("methodName", "" + extras.get("application"));
                previousIntent.putExtra("action", "" + "previous");
                previousIntent.putExtra("deviceid", "" + SharedPrefs.getDeviceId(context));
                android.app.PendingIntent piPrevious =
                        android.app.PendingIntent.getService(
                                context,
                                piId.getAndIncrement(),
                                previousIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                Intent closeIntent = new Intent(context, NotifyActivityHandler.class);
                closeIntent.putExtra("methodName", "" + extras.get("application"));
                closeIntent.putExtra("action", "" + "remove");
                closeIntent.putExtra("deviceid", "" + SharedPrefs.getDeviceId(context));
                android.app.PendingIntent piClose =
                        android.app.PendingIntent.getService(
                                context,
                                piId.getAndIncrement(),
                                closeIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                // Create the intent that happens when pressing the entire notification
                Intent resultIntent = new Intent(context, MainActivity.class);
                android.app.PendingIntent resultPendingIntent =
                        android.app.PendingIntent.getActivity(
                                context,
                                piId.getAndIncrement(),
                                resultIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );
                int mNotificationId;
                android.support.v4.app.NotificationCompat.Builder mBuilder;




                RemoteViews remoteViews = new RemoteViews(MyApp.getContext().getPackageName(),
                        R.layout.notification_three_buttons);

                if (extras.get("application").equals(MainActivity.EIGHTTRACKS.nameAbbrev)) {
                    mBuilder =
                            new android.support.v4.app.NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle((String) extras.get("application"))
                                    .setContentText((String) extras.get("title"))
                                    .setContent(remoteViews);

                    remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_action_white_play);
                    remoteViews.setImageViewResource(R.id.imagenotimiddle, R.drawable.ic_action_white_pause);
                    remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.ic_action_white_next);
                    remoteViews.setImageViewResource(R.id.imagenoticlose, R.drawable.ic_action_cancel);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotileft, piPlay);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotimiddle, piPause);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotiright, piSkip);
                    remoteViews.setOnClickPendingIntent(R.id.imagenoticlose, piClose);
                    remoteViews.setTextViewText(R.id.title, (String) extras.get("application"));
                    remoteViews.setTextViewText(R.id.text, (String) extras.get("title"));


                    mBuilder.setOngoing(true);
                    mNotificationId = MainActivity.EIGHTTRACKS.notificationId;
                }
                else if (extras.get("application").equals(MainActivity.PANDORA.nameAbbrev)) {
                    mBuilder =
                            new android.support.v4.app.NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle((String) extras.get("application"))
                                    .setContentText((String) extras.get("title"))
                                    .setContent(remoteViews);
                    remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_action_white_play);
                    remoteViews.setImageViewResource(R.id.imagenotimiddle, R.drawable.ic_action_white_pause);
                    remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.ic_action_white_next);
                    remoteViews.setImageViewResource(R.id.imagenoticlose, R.drawable.ic_action_cancel);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotileft, piPlay);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotimiddle, piPause);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotiright, piSkip);
                    remoteViews.setOnClickPendingIntent(R.id.imagenoticlose, piClose);
                    remoteViews.setTextViewText(R.id.title, (String) extras.get("application"));
                    remoteViews.setTextViewText(R.id.text, (String) extras.get("title"));

                    mBuilder.setOngoing(true);
                    mNotificationId = MainActivity.PANDORA.notificationId;
                }
                else if (extras.get("application").equals(MainActivity.YOUTUBE.nameAbbrev)) {

                    remoteViews = new RemoteViews(MyApp.getContext().getPackageName(),
                            R.layout.notification_two_buttons);

                    mBuilder =
                            new android.support.v4.app.NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle((String) extras.get("application"))
                                    .setContentText((String) extras.get("title"))
                                    .setContent(remoteViews);

                    remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_action_white_play);
                    remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.ic_action_white_pause);
                    remoteViews.setImageViewResource(R.id.imagenoticlose, R.drawable.ic_action_cancel);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotileft, piPlay);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotiright, piPause);
                    remoteViews.setOnClickPendingIntent(R.id.imagenoticlose, piClose);
                    remoteViews.setTextViewText(R.id.title, (String) extras.get("application"));
                    remoteViews.setTextViewText(R.id.text, (String) extras.get("title"));

                    mBuilder.setOngoing(false);
                    mNotificationId = MainActivity.YOUTUBE.notificationId;

                }
                else if (extras.get("application").equals(MainActivity.VIMEO.nameAbbrev)) {
                    remoteViews = new RemoteViews(MyApp.getContext().getPackageName(),
                            R.layout.notification_two_buttons);
                    mBuilder =
                            new android.support.v4.app.NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle((String) extras.get("application"))
                                    .setContentText((String) extras.get("title"))
                                    .setContent(remoteViews);
                    remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_action_white_play);
                    remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.ic_action_white_pause);
                    remoteViews.setImageViewResource(R.id.imagenoticlose, R.drawable.ic_action_cancel);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotileft, piPlay);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotiright, piPause);
                    remoteViews.setOnClickPendingIntent(R.id.imagenoticlose, piClose);
                    remoteViews.setTextViewText(R.id.title, (String) extras.get("application"));
                    remoteViews.setTextViewText(R.id.text, (String) extras.get("title"));

                    mBuilder.setOngoing(true);
                    mNotificationId = MainActivity.VIMEO.notificationId;
                }
                else if (extras.get("application").equals(MainActivity.PREZI.nameAbbrev)) {
                    remoteViews = new RemoteViews(MyApp.getContext().getPackageName(),
                            R.layout.notification_two_buttons);
                    mBuilder =
                            new android.support.v4.app.NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle((String) extras.get("application"))
                                    .setContentText((String) extras.get("title"))
                                    .setContent(remoteViews);
                    remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_action_white_previous);
                    remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.ic_action_white_next);
                    remoteViews.setImageViewResource(R.id.imagenoticlose, R.drawable.ic_action_cancel);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotileft, piPrevious);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotiright, piNext);
                    remoteViews.setOnClickPendingIntent(R.id.imagenoticlose, piClose);
                    remoteViews.setTextViewText(R.id.title, (String) extras.get("application"));
                    remoteViews.setTextViewText(R.id.text, (String) extras.get("title"));

                    mBuilder.setOngoing(true);
                    mNotificationId = MainActivity.PREZI.notificationId;
                }
                else if (extras.get("application").equals(MainActivity.SPOTIFY.nameAbbrev)) {
                    mBuilder =
                            new android.support.v4.app.NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle((String) extras.get("application"))
                                    .setContentText((String) extras.get("title"))
                                    .setContent(remoteViews);
                    remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_action_white_play);
                    remoteViews.setImageViewResource(R.id.imagenotimiddle, R.drawable.ic_action_white_pause);
                    remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.ic_action_white_next);
                    remoteViews.setImageViewResource(R.id.imagenoticlose, R.drawable.ic_action_cancel);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotileft, piPlay);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotimiddle, piPause);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotiright, piSkip);
                    remoteViews.setOnClickPendingIntent(R.id.imagenoticlose, piClose);
                    remoteViews.setTextViewText(R.id.title, (String) extras.get("application"));
                    remoteViews.setTextViewText(R.id.text, (String) extras.get("title"));

                    mBuilder.setOngoing(true);
                    mNotificationId = MainActivity.SPOTIFY.notificationId;
                }
                else if (extras.get("application").equals(MainActivity.NETFLIX.nameAbbrev)) {
                    remoteViews = new RemoteViews(MyApp.getContext().getPackageName(),
                            R.layout.notification_two_buttons);
                    mBuilder =
                            new android.support.v4.app.NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle((String) extras.get("application"))
                                    .setContentText((String) extras.get("title"))
                                    .setContent(remoteViews);
                    remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_action_white_play);
                    remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.ic_action_white_pause);
                    remoteViews.setImageViewResource(R.id.imagenoticlose, R.drawable.ic_action_cancel);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotileft, piPlay);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotiright, piPause);
                    remoteViews.setOnClickPendingIntent(R.id.imagenoticlose, piClose);
                    remoteViews.setTextViewText(R.id.title, (String) extras.get("application"));
                    remoteViews.setTextViewText(R.id.text, (String) extras.get("title"));

                    mBuilder.setOngoing(true);
                    mNotificationId = MainActivity.NETFLIX.notificationId;
                }
                else if (extras.get("application").equals(MainActivity.IHEARTRADIO.nameAbbrev)) {
                    remoteViews = new RemoteViews(MyApp.getContext().getPackageName(),
                            R.layout.notification_two_buttons);
                    mBuilder =
                            new android.support.v4.app.NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle((String) extras.get("application"))
                                    .setContentText((String) extras.get("title"))
                                    .setContent(remoteViews);
                    remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_action_white_play);
                    remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.ic_action_white_pause);
                    remoteViews.setImageViewResource(R.id.imagenoticlose, R.drawable.ic_action_cancel);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotileft, piPlay);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotiright, piPause);
                    remoteViews.setOnClickPendingIntent(R.id.imagenoticlose, piClose);
                    remoteViews.setTextViewText(R.id.title, (String) extras.get("application"));
                    remoteViews.setTextViewText(R.id.text, (String) extras.get("title"));

                    mBuilder.setOngoing(true);
                    mNotificationId = MainActivity.IHEARTRADIO.notificationId;
                }
                else if (extras.get("application").equals(MainActivity.SLIDESHARE.nameAbbrev)) {
                    remoteViews = new RemoteViews(MyApp.getContext().getPackageName(),
                            R.layout.notification_two_buttons);
                    mBuilder =
                            new android.support.v4.app.NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle((String) extras.get("application"))
                                    .setContentText((String) extras.get("title"))
                                    .setContent(remoteViews);
                    remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_action_white_previous);
                    remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.ic_action_white_next);
                    remoteViews.setImageViewResource(R.id.imagenoticlose, R.drawable.ic_action_cancel);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotileft, piPrevious);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotiright, piNext);
                    remoteViews.setOnClickPendingIntent(R.id.imagenoticlose, piClose);
                    remoteViews.setTextViewText(R.id.title, (String) extras.get("application"));
                    remoteViews.setTextViewText(R.id.text, (String) extras.get("title"));

                    mBuilder.setOngoing(true);
                    mNotificationId = MainActivity.SLIDESHARE.notificationId;
                }
                else if (extras.get("application").equals(MainActivity.GOOGLESLIDES.nameAbbrev)) {
                    remoteViews = new RemoteViews(MyApp.getContext().getPackageName(),
                            R.layout.notification_two_buttons);
                    mBuilder =
                            new android.support.v4.app.NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle((String) extras.get("application"))
                                    .setContentText((String) extras.get("title"))
                                    .setContent(remoteViews);
                    remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_action_white_previous);
                    remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.ic_action_white_next);
                    remoteViews.setImageViewResource(R.id.imagenoticlose, R.drawable.ic_action_cancel);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotileft, piPrevious);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotiright, piNext);
                    remoteViews.setOnClickPendingIntent(R.id.imagenoticlose, piClose);
                    remoteViews.setTextViewText(R.id.title, (String) extras.get("application"));
                    remoteViews.setTextViewText(R.id.text, (String) extras.get("title"));

                    mBuilder.setOngoing(true);
                    mNotificationId = MainActivity.GOOGLESLIDES.notificationId;
                }
                else if (extras.get("application").equals(MainActivity.SONGZA.nameAbbrev)) {
                    mBuilder =
                            new android.support.v4.app.NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle((String) extras.get("application"))
                                    .setContentText((String) extras.get("title"))
                                    .setContent(remoteViews);
                    remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_action_white_play);
                    remoteViews.setImageViewResource(R.id.imagenotimiddle, R.drawable.ic_action_white_pause);
                    remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.ic_action_white_next);
                    remoteViews.setImageViewResource(R.id.imagenoticlose, R.drawable.ic_action_cancel);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotileft, piPlay);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotimiddle, piPause);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotiright, piSkip);
                    remoteViews.setOnClickPendingIntent(R.id.imagenoticlose, piClose);
                    remoteViews.setTextViewText(R.id.title, (String) extras.get("application"));
                    remoteViews.setTextViewText(R.id.text, (String) extras.get("title"));

                    mBuilder.setOngoing(true);
                    mNotificationId = MainActivity.SONGZA.notificationId;
                }
                else if (extras.get("application").equals(MainActivity.SOUNDCLOUD.nameAbbrev)) {
                    mBuilder =
                            new android.support.v4.app.NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle((String) extras.get("application"))
                                    .setContentText((String) extras.get("title"))
                                    .setContent(remoteViews);
                    remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_action_white_play);
                    remoteViews.setImageViewResource(R.id.imagenotimiddle, R.drawable.ic_action_white_pause);
                    remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.ic_action_white_next);
                    remoteViews.setImageViewResource(R.id.imagenoticlose, R.drawable.ic_action_cancel);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotileft, piPlay);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotimiddle, piPause);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotiright, piSkip);
                    remoteViews.setOnClickPendingIntent(R.id.imagenoticlose, piClose);
                    remoteViews.setTextViewText(R.id.title, (String) extras.get("application"));
                    remoteViews.setTextViewText(R.id.text, (String) extras.get("title"));

                    mBuilder.setOngoing(true);
                    mNotificationId = MainActivity.SOUNDCLOUD.notificationId;
                }
                else if (extras.get("application").equals(MainActivity.GROOVESHARK.nameAbbrev)) {
                    mBuilder =
                            new android.support.v4.app.NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle((String) extras.get("application"))
                                    .setContentText((String) extras.get("title"))
//                                    .addAction(R.drawable.ic_action_white_play, "Play", piPlay)
//                                    .addAction(R.drawable.ic_action_white_pause, "Pause", piPause)
//                                    .addAction(R.drawable.ic_action_white_next, "Skip", piSkip)
                                    .setContent(remoteViews);
                    remoteViews.setImageViewResource(R.id.imagenotileft, R.drawable.ic_action_white_play);
                    remoteViews.setImageViewResource(R.id.imagenotimiddle, R.drawable.ic_action_white_pause);
                    remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.ic_action_white_next);
                    remoteViews.setImageViewResource(R.id.imagenoticlose, R.drawable.ic_action_cancel);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotileft, piPlay);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotimiddle, piPause);
                    remoteViews.setOnClickPendingIntent(R.id.imagenotiright, piSkip);
                    remoteViews.setOnClickPendingIntent(R.id.imagenoticlose, piClose);
                    remoteViews.setTextViewText(R.id.title, (String) extras.get("application"));
                    remoteViews.setTextViewText(R.id.text, (String) extras.get("title"));

                    mBuilder.setOngoing(true);
                    mNotificationId = MainActivity.GROOVESHARK.notificationId;
                }
                else {
                    return;
                    // only specific apps should have notifications, not Yelp for example.
//                    mBuilder =
//                            new android.support.v4.app.NotificationCompat.Builder(context)
//                                    .setSmallIcon(R.drawable.logo)
//                                    .setContentTitle((String) extras.get("application"))
//                                    .setContentText((String) extras.get("title"));
//                    mBuilder.setOngoing(true);
//                    mNotificationId = 006;
                }

                mBuilder.setContentIntent(resultPendingIntent);


                // Gets an instance of the NotificationManager service
                android.app.NotificationManager mNotifyMgr =
                        (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                // Builds the notification and issues it.
                mNotifyMgr.notify(mNotificationId, mBuilder.build());
            }
            /*************** REMOVE NOTIFICATIONS ****************/
            else if (extras.containsKey(("action")) && extras.get("action").equals("remove")) {
                // Gets an instance of the NotificationManager service
                Log.v("Notification", "Trying to remove a notification " + extras.get("application"));
                android.app.NotificationManager mNotifyMgr =
                        (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                if (extras.get("application").equals(MainActivity.EIGHTTRACKS.nameAbbrev)) {
                    mNotifyMgr.cancel(MainActivity.EIGHTTRACKS.notificationId);
                }
                else if (extras.get("application").equals(MainActivity.PANDORA.nameAbbrev)) {
                    mNotifyMgr.cancel(MainActivity.PANDORA.notificationId);
                }
                else if (extras.get("application").equals(MainActivity.YOUTUBE.nameAbbrev)) {
                    mNotifyMgr.cancel(MainActivity.YOUTUBE.notificationId);
                }
                else if (extras.get("application").equals(MainActivity.VIMEO.nameAbbrev)) {
                    mNotifyMgr.cancel(MainActivity.VIMEO.notificationId);
                }
                else if (extras.get("application").equals(MainActivity.PREZI.nameAbbrev)) {
                    mNotifyMgr.cancel(MainActivity.PREZI.notificationId);
                }
                else if (extras.get("application").equals(MainActivity.SPOTIFY.nameAbbrev)) {
                    mNotifyMgr.cancel(MainActivity.SPOTIFY.notificationId);
                }
                else if (extras.get("application").equals(MainActivity.NETFLIX.nameAbbrev)) {
                    mNotifyMgr.cancel(MainActivity.NETFLIX.notificationId);
                }
                else if (extras.get("application").equals(MainActivity.IHEARTRADIO.nameAbbrev)) {
                    mNotifyMgr.cancel(MainActivity.IHEARTRADIO.notificationId);
                }
                else if (extras.get("application").equals(MainActivity.SLIDESHARE.nameAbbrev)) {
                    mNotifyMgr.cancel(MainActivity.SLIDESHARE.notificationId);
                }
                else if (extras.get("application").equals(MainActivity.GOOGLESLIDES.nameAbbrev)) {
                    mNotifyMgr.cancel(MainActivity.GOOGLESLIDES.notificationId);
                }
                else if (extras.get("application").equals(MainActivity.SONGZA.nameAbbrev)) {
                    mNotifyMgr.cancel(MainActivity.SONGZA.notificationId);
                }
                else if (extras.get("application").equals(MainActivity.SOUNDCLOUD.nameAbbrev)) {
                    mNotifyMgr.cancel(MainActivity.SOUNDCLOUD.notificationId);
                }
                else if (extras.get("application").equals(MainActivity.GROOVESHARK.nameAbbrev)) {
                    mNotifyMgr.cancel(MainActivity.GROOVESHARK.notificationId);
                }
                else  {

                }
            }
            /***************END MAKE NOTIFICATION****************/

//
//            if (extras.getString("title") != null) {
//                if (extras.getString("title").equals("Email")) {
//                    Log.w("Yo", "Seems like you've opened your email.");
//                    Toast.makeText(context, "EMAIL", Toast.LENGTH_LONG).show();
//                } else if (extras.getString("title").equals("8tracks")) {
//                    Log.w("Yo", "Seems like you've opened your music.");
//                    Toast.makeText(context, "8TRACKS", Toast.LENGTH_LONG).show();
//                } else {
//                    Toast.makeText(context, extras.getString("title"), Toast.LENGTH_LONG).show();
//                }
//            } else {
//                Toast.makeText(context, "no title", Toast.LENGTH_LONG).show();
//            }

            completeWakefulIntent(intent);
        }
    }
}