package com.example.stephencognetta.amico;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by stephencognetta on 4/19/15.
 */
public class OngoingNotificationListenerService extends WearableListenerService {
    public static final String START_ACTIVITY = "/start_activity";

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        String data = new String(messageEvent.getData());
        Log.v("Yoooo", data);

        Intent pauseIntent = new Intent(this, NotifyActivityHandler.class);
        pauseIntent.putExtra("methodName", "" + data.split(";")[0]);
        pauseIntent.putExtra("action", "" + data.split(";")[1]);
        pauseIntent.putExtra("deviceid", "" + SharedPrefs.getDeviceId(this));
        pauseIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startService(pauseIntent);

    }
}
