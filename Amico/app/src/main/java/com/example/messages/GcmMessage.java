package com.example.messages;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.example.stephencognetta.amico.AlarmReceiver;
import com.example.stephencognetta.amico.MainActivity;
import com.example.stephencognetta.amico.SharedPrefs;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class GcmMessage {

    public static final String PROJECT_NUMBER = "596039776570";

    static final String APPLICATION = "application";
    static final String TYPE = "type";
    static final String ACTION = "action";
    static final String MESSAGETYPE = "messagetype";
    static final String DEVICETYPE = "devicetype";
    static final String PIN = "pin";
    static final String DEVICEID = "deviceid";

    private static AtomicInteger msgId = new AtomicInteger();

    private final Context context;
    private final String messageType;

    protected HashMap<String, String> contents = new HashMap<String, String>();

    public GcmMessage(
            Context context,
            String messageType) {

        if (context == null || messageType == null) {
            throw new NullPointerException("GcmMessage constructor arguments null.");
        }
        this.context = context;
        this.messageType = messageType;
    }

    public GcmMessage addParam(String name, String value) {
        contents.put(name, value);
        return this;
    }

    public void send() {

        Log.v("GCM", "Sending " + messageType + " Message");
        Log.v("GCM", "Contents: " + toString());

        Calendar c = Calendar.getInstance();
        SharedPrefs.timeSinceSent = c.getTimeInMillis();


        contents.put(MESSAGETYPE, messageType);
        contents.put(DEVICEID, SharedPrefs.getDeviceId(context));

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    String msg = "";
                    Bundle bundle = new Bundle();
                    for (Map.Entry<String, String> param : contents.entrySet()) {
                        bundle.putString(param.getKey(), param.getValue());
                        msg += "\n" + param.getKey() + ": " + param.getValue();
                    }
                    String id = Integer.toString(msgId.incrementAndGet());

                    GoogleCloudMessaging.getInstance(context).send(PROJECT_NUMBER + "@gcm.googleapis.com", id, bundle);

                    Log.v("GCM", "Sent message" + msg);
                } catch (IOException ex) {
                    Log.v("GCM", "Error :" + ex.getMessage());
                }
                return null;
            }
            @Override protected void onPostExecute(Void msg) {}
        }.execute(null, null, null);
    }

    public String toString() {
        return contents.toString();
    }
}
