package com.example.messages;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.HashMap;

/**
 * Created by eugene on 1/6/15.
 */
public class ActionMessage extends GcmMessage {
    static final String messageType = "browser_action";

    public ActionMessage(
            Context context,
            String application,
            String action) {

        super(context, messageType);
        if (application == null || action == null) {
            throw new NullPointerException("Action Message fields not all set.");
        }

        contents.put(APPLICATION, application);
        contents.put(ACTION, action);
    }
}
