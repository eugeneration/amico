package com.example.messages;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.HashMap;

/**
 * Created by eugene on 1/6/15.
 */
public class PairingMessage extends GcmMessage{
    static final String messageType = "pairing";

    static final String devicetype = "android";
    static final String action = "finish";

    public PairingMessage (
            Context context,
            String pairing_pin) {

        super(context, messageType);
        if (pairing_pin == null) {
            throw new NullPointerException("pairing_pin cannot be null.");
        }
        contents.put(ACTION, action);
        contents.put(DEVICETYPE, devicetype);
        contents.put(PIN, pairing_pin);
    }
}