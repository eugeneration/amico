package com.example.messages;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.HashMap;

/**
 * Created by eugene on 1/6/15.
 */
public class RefreshMessage extends GcmMessage {
    static final String messageType = "refresh";
    public RefreshMessage(Context context) {
        super(context, messageType);
    }
}
