package com.example.stephencognetta.amico;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by stephencognetta on 4/19/15.
 */
public class OngoingNotificationListenerService extends WearableListenerService {
    public static final String START_ACTIVITY = "/start_activity";

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        String data = new String(messageEvent.getData());
        Log.v("Yoooo", data);

        //parse the data
        String clean = data.split("\\{")[1];
        String cleaner = clean.split("\\}")[0];
        //Log.v("Yoooclean", cleaner);
        String[] parts = cleaner.split(",");

        String title = "";
        String application = "";

        for (int i = 0; i < parts.length; i++) {
            //Log.v("parts", parts[i].split("=")[0]);
            if((parts[i].split("=")[0]).equals(" application")) {
                //Log.v("Yoo", "Your application is: " + parts[i].split("=")[1]);
                application = parts[i].split("=")[1];
            }
            if((parts[i].split("=")[0]).equals(" title")) {
                //Log.v("Yoo", "Your title is: " + parts[i].split("=")[1]);
                title = parts[i].split("=")[1];
            }
        }

        if( messageEvent.getPath().equalsIgnoreCase( START_ACTIVITY ) ) {
            Intent notificationIntent = new Intent(this, com.example.stephencognetta.amico.NotificationActivity.class);
            PendingIntent notificationPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            MainActivity.initGoogleApiClient(this);

            Context context = getApplicationContext();
            Log.v("yo", "making notification");
            Notification notification =
                    new Notification.Builder(context)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle(application)
                            .setContentText(title)
                            .setOngoing(true)
                            .extend(new Notification.WearableExtender()
                                    .addAction(new Notification.Action.Builder(R.drawable.ic_action_play, "Action", notificationPendingIntent).build())
                                    .setContentAction(0)
                                    .setContentIcon(R.drawable.ic_launcher)
                                    .setCustomSizePreset(Notification.WearableExtender.SIZE_LARGE)
                                    .setStartScrollBottom(false)
                                    .setHintHideIcon(true))

                            .build();

            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

            notificationManager.notify(0, notification);
            Log.v("yo", application + " " + title);
        } else {
            super.onMessageReceived( messageEvent );
        }
    }
}
