package com.example.stephencognetta.amico;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.wearable.view.CardFragment;
import android.util.Log;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;


/**
 * Created by stephencognetta on 4/19/15.
 */
public class NotificationActivity extends Activity {


    private void sendMessage( final String path, final String text ) {
        Log.i("yooo", "messaaaaage");
        if(MainActivity.mApiClient.isConnected()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.d("Yooo", "Sending message like i said");
                    NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(MainActivity.mApiClient).await();
                    for (Node node : nodes.getNodes()) {
                        Log.i("yoooo", node.getDisplayName());
                        Wearable.MessageApi.sendMessage(
                                MainActivity.mApiClient, node.getId(), path, text.getBytes()).setResultCallback(
                                new ResultCallback<MessageApi.SendMessageResult>() {
                                    @Override
                                    public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                        if (!sendMessageResult.getStatus().isSuccess()) {
                                            Log.e("Yoo", "Failed to send message with status code: "
                                                    + sendMessageResult.getStatus().getStatusCode());
                                        } else {
                                            Log.i("yoooo", "success!!: " );
                                        }
                                    }
                                }
                        );
                    }

                }
            }).start();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*setContentView(R.layout.activity_notification);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        CardFragment cardFragment = CardFragment.create(getString(R.string.app_name),
                getString(R.string.app_name),
                R.drawable.ic_launcher);
        fragmentTransaction.add(R.id.frame_layout, cardFragment);
        fragmentTransaction.commit();*/

        sendMessage(OngoingNotificationListenerService.START_ACTIVITY,"youtube;pause");
    }
}
