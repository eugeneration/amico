#!/usr/bin/python
from __future__ import division # for integer division, do //
import sys, json, time, random, string, xmpp, sqlite3, uuid
import subprocess, errno, traceback
from datetime import datetime
from datetime import timedelta

applications = {
  "eighttracks": {
      "browser_action": {
        "pause": {}, 
        "play": {}, 
        "skip": {}
      },
      "android_card": {
        "add": {
          "required": ['title'],
        },
        "remove":{},
        "update": {
          "required": ['title'],
        }
      }
    },
  # =========================================================================
  "googleslides": {
    "browser_action": {
      "next": {}, 
      "previous": {}
    },
    "android_card": {
      "add": {
        "required": ['title'],
      },
      "remove":{},
      "update": {
        "required": ['title'],
      }
    },
  },
  # =========================================================================
  "grooveshark": {
      "browser_action": {
        "pause": {}, 
        "play": {}, 
        "skip": {}
      },
      "android_card": {
        "add": {
          "required": ['title'],
        },
        "remove":{},
        "update": {
          "required": ['title'],
        }
      }
    },
  # =========================================================================
  "iheartradio": {
      "browser_action": {
        "pause": {}, 
        "play": {}, 
      },
      "android_card": {
        "add": {
          "required": ['title'],
        },
        "remove":{},
        "update": {
          "required": ['title'],
        }
      }
    },
  # =========================================================================
  "netflix": {
      "browser_action": {
        "pause": {}, 
        "play": {}, 
      },
      "android_card": {
        "add": {
          "required": ['title'],
        },
        "remove":{},
        "update": {
          "required": ['title'],
        }
      }
    },
  # =========================================================================
  "pandora": {
      "browser_action": {
        "pause": {}, 
        "play": {}, 
        "skip": {}
      },
      "android_card": {
        "add": {
          "required": ['title'],
        },
        "remove":{},
        "update": {
          "required": ['title'],
        }
      }
    },
  # =========================================================================
  "prezi": {
    "browser_action": {
      "next": {}, 
      "previous": {}
    },
    "android_card": {
      "add": {
        "required": ['title'],
      },
      "remove":{},
      "update": {
        "required": ['title'],
      }
    },
  },
  # =========================================================================
  "slideshare": {
    "browser_action": {
      "next": {}, 
      "previous": {}
    },
    "android_card": {
      "add": {
        "required": ['title'],
      },
      "remove":{},
      "update": {
        "required": ['title'],
      }
    },
  },
  # =========================================================================
  "songza": {
    "browser_action": {
      "pause": {}, 
      "play": {},
      "skip": {}
    },
    "android_card": {
      "add": {
        "required": ['title'],
      },
      "remove":{},
      "update": {
        "required": ['title'],
      }
    },
  },
  # =========================================================================
  "soundcloud": {
    "browser_action": {
      "pause": {}, 
      "play": {},
      "skip": {}
    },
    "android_card": {
      "add": {
        "required": ['title'],
      },
      "remove":{},
      "update": {
        "required": ['title'],
      }
    },
  },
  # =========================================================================
  "spotify": {
    "browser_action": {
      "pause": {}, 
      "play": {},
      "skip": {}
    },
    "android_card": {
      "add": {
        "required": ['title'],
      },
      "remove":{},
      "update": {
        "required": ['title'],
      }
    },
  },

  # =========================================================================
  "vimeo": {
    "browser_action": {
      "pause": {},
      "play": {},
    },
    "android_card": {
      "add": {
        "required": ['title'],
      },
      "remove":{},
      "update": {
        "required": ['title'],
      }
    },
  },
  # =========================================================================
  "youtube": {
    "browser_action": {
      "pause": {},
      "play": {},
    },
    "android_card": {
      "add": {
        "required": ['title'],
      },
      "remove":{},
      "update": {
        "required": ['title'],
      }
    }
  },
  # =========================================================================
  "yelp": {
    "android_card": {
      "add": {
        "required": ['latlng'],
      },
      "remove":{},
      "update": {
        "required": ['title'],
      }
    }
  },
  # =========================================================================
  "docs": {
    "browser_action": {
      "place_image": {
        "required": ["image_src"],
      }
    },
    "android_card": {
      "add": {
        "required": ['title'],
      },
      "remove":{},
      "update": {
        "required": ['title'],
      }
    }
  }
}


SERVER = 'gcm.googleapis.com'
PORT = 5235
USERNAME = "596039776570"
PASSWORD = "AIzaSyCGZhlSrNd6gz1JVU1errLhBTSbsYYohdU"

#Eugene
E_ANDROID = "APA91bHfP4cy4WZg4pAhg-0PNqJL0WNDrWLB_JsC97zxykczaLRdnQzN6PDoRoLZG123gyPrSnpiJ03AQxwsrTSbvqdDaFLQhompFoJA82-4oacMA88L39ppsFIW4mB7K_gSnp-VInItE8ZN_ij495XTziS74r1O2Q"
E_CHROME = "APA91bGUOyKeT-ZO2i0TgQHO1lxZk0v3r0_N37BYvAmseFSFfFtNAa8t5B-aeYtP8Kll4bHbZ7W7qv3uk5NHFcy8gagiNxmnduzgg71ThGO6HNFZSkLspKl7JPb-g3VKwLlK9x5UoV2u41CorecwH-a1X7xgkdiyNQ"

#STEPHEN
S_ANDROID = "APA91bGzZ2HrvaZujm9LPDdidVheisf16B7MKqBpnfqOk2BVyz3tW6qZ8PAWEo1Li_eAvFYToIAiTnEBbFr6-DYVuHDYXbqy3Gem8sJ6ey81GUufWXepaHYBsavhcmflR6-J-604DIo0gXC9l80rZJSZlcCPBAQlqw"
S_CHROME = "APA91bHRym3UKBcve38gXxL_Xdbn0pdMreI3AsTXhPSnbvfH1kGeez8YMrWvJyPN8eyAdvY5F4yKZdU7AKFrzShpHi-KnOruK_1wtfp_B72r2zq_Ln2zVNMtKESYtg_ia6_mPv23e5qlemxQ2N8S5BK1NcKv44jPOQ"

unacked_messages_quota = 100
send_queue = []

# Timed Pairing Map
# contains secret code, time, regid, device type, username (default blank)
# send the username along with every message sent.
# if the username / regid doesn't match on the server, it will be deleted
# if a client is sent a message with a username it doesn't recognize, it will
#   ask the server to delete it from that account.
#   This prevents a user from deleting the app, reinstalling it, and expecting
#   it to be completely unconnected.
pairing_map = {}
PAIRING_TIME_LIMIT = 60*60*24 # time in seconds = 1 day

# XMPP Heartbeat
# necessary to avoid disconnections
heartbeat_interval = 60 * 30 # time in seconds = 30 minutes

draining = False;

# Return a random alphanumerical id
def random_id():
  rid = ''
  for x in range(8): rid += random.choice(string.ascii_letters + string.digits)
  return rid

# Return an id for device id and group name
def generate_id():
  return uuid.uuid1().hex

def message_callback(session, message):
  global unacked_messages_quota

  gcm = message.getTags('gcm')
  if not gcm:
    return

  gcm_json = gcm[0].getData()
  msg = json.loads(gcm_json)

  # Print a time stamp for each message
  print datetime.now()

  #=====================================================================
  # NACK type responses
  if 'message_type' in msg:
    if msg['message_type'] == 'ack' or msg['message_type'] == 'nack':
      print "^Ack/Nack"
      unacked_messages_quota += 1

    # Google will drop this connection - fire up a new connection
    elif msg['message_type'] == 'control' and msg['control_type'] == 'CONNECTION_DRAINING':
      print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
      print "WHOOOOAAAAAAAA!!!!!!!!!!!!!!!!!!"
      print "CONNECTION DRAINING"
      print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

      create_new_server()
      draining = True
      # kill this process when you get a type of nack

  #=====================================================================
  # ACTUAL MESSAGES
  else:
    # Acknowledge the incoming message immediately.
    sender_regid = msg['from']
    send({'to': sender_regid,
          'message_type': 'ack',
          'message_id': msg['message_id'],
          })
    print "^Ack"

    sender_regid = msg['from']
    send({'to': sender_regid,
          'message_type': 'test ack',
          'message_id': msg['message_id'],
          'other_field': 'hi',
          })
    print "test Ack"

    # If it's a valid message
    if 'data' in msg and 'messagetype' in msg['data']:
      data = msg['data']
      messagetype = data.get('messagetype')

      sender_deviceid = data.get('deviceid')
      data['regid'] = sender_regid

      #=====================================================================
      # PAIRING SET UP
      #=====================================================================
      if messagetype == 'pairing':
        print "PAIR MESSAGE"
        handle_pairing_message(data)

      #=====================================================================
      # UNPAIRING
      #=====================================================================
      elif messagetype == 'unpair':
        print "UNPAIR MESSAGE"
        #unpairDevice(deviceid, regid, userid)

      #=====================================================================
      # REFRESH
      #=====================================================================
      elif messagetype == 'refresh':
        print "REFRESH MESSAGE"
        deviceid, regid, devicetype = findRecipient(sender_deviceid, sender_regid, messagetype)
        data['deviceid'] = deviceid
        data.pop('regid', None)
        if regid is not None:
          send_queue.append({'to': regid,
                           'message_id': random_id(),
                           'data': data})

      #=====================================================================
      # ACTION MESSAGES - Needs to be rerouted
      #=====================================================================
      else:
        print "ACTION MESSAGE"
        handle_action_message(data)

#=====================================================================
# PAIRING MESSAGES
#=====================================================================

def pin_exists(pairing_pin, time):
  if (pairing_pin in pairing_map):
    td = time - pairing_map[pairing_pin]['time']
  else:
    td = timedelta.min

  # total_seconds() doesn't work in earlier versions of python (EC2)
  return (pairing_pin in pairing_map and getSeconds(td) < PAIRING_TIME_LIMIT)

def getSeconds(td):
  return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6

def handle_pairing_message(data):

  pairing_pin = data.get('pin')
  action = data.get('action')

  # check message format
  if not (pairing_pin and action and "devicetype" in data):
    print "Incorrect Registration Message structure"
    return

  time = datetime.now()

  # TODO: send failure messages

  if action == "start":
    if pin_exists(pairing_pin, time):
      # Pairing Failure:
      # in the scenario that someone pairing at the same time had the
      # same pin, send a failure message
      print "PAIRING FAILURE - OVERLAPPING PIN"

    else:
      print "PAIRING START: ", pairing_pin
      data['time'] = time
      pairing_map[pairing_pin] = data

  elif action == "finish":
    if pin_exists(pairing_pin, time):
      print "PAIRING FINISH"
      pair_devices(data, pairing_map[pairing_pin])
      pairing_map.pop(pairing_pin, None)

    else:
      # Pairing Failure:
      if pairing_pin in pairing_map:
        print "PAIRING FAILURE - PAIRING TIMEOUT"
        pairing_map.pop(pairing_pin, None)
      else:
        print "PAIRING FAILURE - INCORRECT CODE"


  ######################################################################

# finds the other device of the user
# TODO: Currently does not support >2 devices
def findRecipient(deviceid, regid, messagetype):
  userid = getUserOfDevice(deviceid, regid)

  c.execute('''SELECT deviceid, regid, devicetype FROM devices WHERE userid=? AND deviceid!=? AND regid!=?''',
      (userid, deviceid, regid))
  row = c.fetchone()

  print "recipient", row
  if row is None or row[1] is None:
    print "No recipient found with userid:", userid, ", deviceid: ", deviceid
    return (None, None, None)
  else:
    return row

  ######################################################################

# searches for a user with a device with either the same regid or deviceid
# if only one match, should remove device and request a repair
def getUserOfDevice(deviceid, regid):
  c.execute('''SELECT userid FROM devices WHERE deviceid=? AND regid=?''', (deviceid, regid))
  row = c.fetchone()
  if row is not None:
    return row[0]
  else:
    return None
  # if fail:
  #  c.execute('''SELECT userid FROM devices WHERE deviceid="device1" OR regid="regid1"''')
  #  // if succeed, ask for a change
  #  // if fail, fail

# Creates a new user in the database with a random userid
def createNewUser():
  userid = generate_id()
  c.execute('''INSERT INTO users VALUES (?, 0, 0)''', userid)
  conn.commit()
  return userid

# Adds a device to a user
# Will overwrite a device with the same regid or deviceid
def addDevice(deviceid, regid, devicetype, userid):
  c.execute('''INSERT INTO devices VALUES (?, ?, ?, ?)''', (deviceid, regid, devicetype, userid))
  conn.commit()
  return True

# Removes a device from a user
# Looks for any match, deviceid or userid, as a change might have occured
def removeDevice(deviceid, regid, userid):
  c.execute('''DELETE FROM DEVICES WHERE deviceid=? AND regid=? AND userid=?''', (deviceid, regid, userid))
  conn.commit()
  # remove user if there are 0 devices attached to that user
  return True

# Delivers the deviceid to the a device
def sendSuccessfulPairingMessage(deviceid, regid):
  print "Sending Registration Successful message"
  send_queue.append({'to': regid, # chrome
    'message_id': random_id(),
    'data': {
      "messagetype": "pairing",
      "deviceid": deviceid
    }})

# commands two devices to perform a pairing test
# def sendPairTestCommand(regidA, deviceidA, regidB, deviceidB):

# commands a device to repair
# Device should have been deleted from database, but this is not necessary
#def sendRepairCommand(regid, deviceid):

# increment the message counter
#def onSuccessfulMessage(messagetype, userid):

#Get both device data objects
#See if there is an existing device id for either of them
#If there is, try to find the corresponding user id. Add the other device id + reg id pair
#Else, new user with a random user id. Add both device id + reg id pairs
#Send successful pairing message to both
#On successful pairing, ask user to send message from one device to the other (swipe on phone?)

def pair_devices(dataA, dataB):
  userid = None
  deviceidA = dataA.get('deviceid') or generate_id()
  deviceidB = dataB.get('deviceid') or generate_id()
  regidA = dataA['regid']
  regidB = dataB['regid']

  if 'deviceid' in dataA:
    userid = getUserOfDevice(deviceidA, regidA)
  if 'deviceid' in dataB:
    userid = getUserOfDevice(deviceidB, regidB)

  userid = userid or generate_id()

  if (addDevice(deviceidA, regidA, dataA['devicetype'], userid) and
      addDevice(deviceidB, regidB, dataB['devicetype'], userid)):
    sendSuccessfulPairingMessage(deviceidA, dataA['regid'])
    sendSuccessfulPairingMessage(deviceidB, dataB['regid'])

  else:
    print "Unable to pair"

#=====================================================================
# UNPAIR MESSAGES
#=====================================================================
def handle_unpair_message(data):
  print "UNPAIR MESSAGE"

#=====================================================================
# REGISTRATION MESSAGES
#=====================================================================
def handle_action_message(data):
  print "ACTION MESSAGE"
  messagetype = data.get('messagetype')
  application = data.get('application')
  action = data.get('action')

  # ensure the message is correctly formatted
  # all messages require messagetype, application, and action
  # then they must also have the data types stored in the JSON file
  if not(application and messagetype and action and 'deviceid' in data and 'regid' in data):
    print "Incorrect Action Message structure"
    return
  # check the requirements
  if application not in applications:
    print "Unknown application:", application
    return
  if messagetype not in applications[application]:
    print "Unsupported message type", messagetype, "for", application
    return
  if action not in applications[application][messagetype]:
    print "Unsupported action", action, "of type", messagetype, "for", application
    return
  for required in applications[application][messagetype][action].get('required', {}):
    if required not in data:
      print "Missing required", required, "for", application, messagetype, action
      return

  # correct for ampersand
  if data.has_key('title'):
    data['title'] = data['title'].replace('&', '+')

  # send to the other device
  sender_deviceid = data.get('deviceid')
  sender_regid = data.get('regid')
  deviceid, regid, devicetype = findRecipient(sender_deviceid, sender_regid, messagetype)
  if regid is None:
    print "No recipient found"
    return

  # send the message
  data['deviceid'] = deviceid
  data.pop('regid', None)
  send_queue.append({'to': regid,
                     'message_id': random_id(),
                     'data': data})

  print data

def send(json_dict):
  global last_sent_message_time
  last_sent_message_time = datetime.now(); # reset heartbeat timer
  print last_sent_message_time # print sent message times for logging
  template = ("<message><gcm xmlns='google:mobile:data'>{1}</gcm></message>")
  client.send(xmpp.protocol.Message(
      node=template.format(client.Bind.bound[0], json.dumps(json_dict))))

# checks if it needs to send a heart beat and sends one
def send_heartbeat():
  global last_sent_message_time
  current_time = datetime.now()
  if (getSeconds(current_time - last_sent_message_time) > heartbeat_interval):
    last_sent_message_time = current_time
    print last_sent_message_time, "HEARTBEAT"
    client.send(" ")


def flush_queued_messages():
  global unacked_messages_quota
  while len(send_queue) and unacked_messages_quota > 0:
    send(send_queue.pop(0))
    unacked_messages_quota -= 1

def on_disconnect():
  print '\n'
  print 'Looks like we lost Connection! Killing Server.'
  print '\n'

  # the disconnect occured for some other reason. Kill this server and
  # create a new one, because one wasn't created yet
  if not draining:
    print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
    print "WHAT HAPPENED TO MMEEEEEEEEE!!!!!!"
    print "the death of script.py reverberated across the world."
    print "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
    create_new_server()

  sys.exit(1)

def create_new_server():
  subprocess.call('./run.sh', shell=True)

# connect to the database
conn = sqlite3.connect('registrations.db')
c = conn.cursor()
              
# set up XMPP GCM client
client = xmpp.Client('gcm.googleapis.com', debug=['socket'])
client.connect(server=(SERVER,PORT), secure=1, use_srv=False)
auth = client.auth(USERNAME, PASSWORD)
if not auth:
  print 'Authentication failed!'
  sys.exit(1)

client.RegisterHandler('message', message_callback)
client.RegisterDisconnectHandler(on_disconnect)
draining = False

last_sent_message_time = datetime.now()

# Continuously process messages
while True:
  try:
    client.Process(1)
    flush_queued_messages()
    send_heartbeat()

  except xmpp.protocol.SeeOtherHost:
    traceback.print_exc()

    print "\n\n"
    print "mysterious SeeOtherHost exception. Redirects to the same server..."
    print "\n\n"

    create_new_server()
    sys.exit(1)